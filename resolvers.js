const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const cloudinary = require("cloudinary");
const nodemailer = require("nodemailer");
const validator = require("validator");
const moment = require("moment");
const { gqlSuccesObj, gqlErrorObj } = require("./api/constants/Message");

const transporter = nodemailer.createTransport({
  service: process.env.MAIL_SERVICE,
  auth: {
    user: process.env.MAIL_EMAIL,
    pass: process.env.MAIL_KEY
  }
});

const sendMAil = mailOptions => {
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUD_KEY,
  api_secret: process.env.CLOUD_SECRET
});

const createToken = (person, secret, expiresIn = null) => {
  const { familyName, email, _id } = person["user"];
  if (expiresIn !== null)
    return jwt.sign({ familyName, email, _id, type: person["type"] }, secret, {
      expiresIn
    });

  return jwt.sign({ familyName, email, _id, type: person["type"] }, secret);
};

exports.resolvers = {
  Query: {
    getCurrentUser: async (root, args, { currentUser, User }) => {
      if (!currentUser) return null;
      const user = await User.findOne({ _id: currentUser._id });
      const retval = await Object.assign(user, { type: currentUser.type });
      return retval;
    }
  },

  Mutation: {
    signupUser: async (root, { familyName, email, password }, { User }) => {
      familyName = familyName.toLowerCase();
      email = email.toLowerCase();
      const user = await User.findOne({ familyName });
      const _email = await User.findOne({ email });

      if (user) return { ...gqlErrorObj, message: "User already exists" };
      if (_email) return { ...gqlErrorObj, message: "Email already exists" };
      sendMAil({
        from: process.env.MAIL_EMAIL,
        to: email,
        subject: "Sending Email using Node.js",
        html: "<h1>Welcome</h1><p>That was easy!</p>"
      });

      const newUser = await new User({
        familyName,
        email,
        password
      }).save();
      return {
        token: "User created, Might be good to send verify email action"
      };
    },

    signinUser: async (root, { email, password }, { User }) => {
      email = email.toLowerCase();
      let user;
      // if (email.indexOf('@') > 0)  user = await User.findOne({ email });
      if (validator.isEmail(email)) user = await User.findOne({ email });
      else user = await User.findOne({ familyName: email });
      if (!user) return { ...gqlErrorObj, message: "User not found" };

      const isValidPassword = await bcrypt.compare(password, user.password);
      if (!isValidPassword)
        return { ...gqlErrorObj, message: "Invalid password" };

      return {
        ...gqlSuccesObj,
        token: createToken({ user, type: "parent" }, process.env.SECRET, null),
        type: "parent"
      };
    },

    loginKid: async (root, { id, pass }, { User }) => {
      // get family name and childId
      const username = id.substr(0, id.indexOf("@")).toLowerCase();
      const familyName = id.substr(id.indexOf("@") + 1, id.length);

      // get kids object fro array and compare password
      const user = await User.findOne(
        { familyName },
        { kids: { $elemMatch: { username, pass } } }
      );

      if (!user) return { ...gqlErrorObj, message: "User not found" };
      if (user["kids"].length < 1)
        return { ...gqlErrorObj, message: "Username or Password not correct" };
      // better still create child token
      return {
        ...gqlSuccesObj,
        token: jwt.sign(
          { familyName, username, _id: user._id, type: "kid" },
          process.env.SECRET
        ),
        type: "kid",
        username
      };
    },

    userAvatar: async (root, { file }, { currentUser, User }) => {
      if (!currentUser) return null;
      const _id = currentUser._id;
      let imgUrl;
      await cloudinary.v2.uploader.upload(
        `${file}`,
        {
          public_id: `thumbnail/${_id}`,
          resource_type: "image",
          format: "jpg",
          quality: 80,
          width: 100,
          height: 100,
          crop: "fill",
          radius: "max"
        },
        (error, result) => {
          if (error) return "error";
          imgUrl = result["secure_url"];
        }
      );

      const user = await User.findOneAndUpdate(
        { _id },
        { $set: { avatar: imgUrl } }
      );
      return user;
    },

    addHouseRule: async (root, { houserules }, { currentUser, User }) => {
      if (!currentUser) return null;
      const _id = currentUser._id;
      await User.findOneAndUpdate({ _id }, { $set: { houserules } });
      return (user = await User.findOne({ _id }));
    },

    addChore: async (
      root,
      { name, end, start, points, todos, connectedKids },
      { currentUser, User }
    ) => {
      if (!currentUser) return null;
      const _id = currentUser._id;
      await User.findOneAndUpdate(
        { _id },
        {
          $addToSet: {
            choreTodo: {
              id: Date.now(),
              name,
              end,
              start,
              points,
              todos,
              connectedKids
            }
          }
        }
      );
      return (user = await User.findOne({ _id }));
    },

    addFamilyMember: async (
      root,
      { name, chores, pass, username },
      { currentUser, User }
    ) => {
      const usernameLower = username.toLowerCase();
      if (!currentUser) return null;
      const _id = currentUser._id;
      return (user = await User.findOneAndUpdate(
        { _id },
        { $addToSet: { kids: { name, chores, pass, username: usernameLower } } }
      ));
    },

    addKidSubjects: async (root, { subject }, { currentUser, User }) => {
      if (!currentUser) return null;
      const _id = currentUser._id;
      parent = await User.findOne({ _id });
      if (!parent) throw new Error("User not found");

      const child = await User.findOne({
        _id,
        kids: { $elemMatch: { username: currentUser.username } }
      });
      const currentKid = child["kids"].map(el => {
        if (el["username"] === currentUser.username) {
          el["subjects"] = subject;
          return el;
        }
        return el;
      });

      return (user = await User.findOneAndUpdate(
        { _id },
        { $set: { kids: currentKid } },
        { new: true }
      ));
    },

    updateChildPoint: async (
      root,
      { end, start, title, month, password, color, gameTime },
      { currentUser, User }
    ) => {
      if (!currentUser) return null;
      parent = await User.findOne({ _id: currentUser._id });
      if (!parent) throw new Error("User not found");

      const isValidPassword = await bcrypt.compare(password, parent.password);
      if (!isValidPassword) throw new Error("Invalid password");
      // Check if last month exist in the points array
      const child = await User.findOne({
        _id: currentUser._id,
        kids: { $elemMatch: { username: currentUser.username } }
      });
      const currentKid = child["kids"].filter(
        el => el["username"] === currentUser.username
      );
      const newGameTime = +(currentKid[0]["gameTime"] || 0) + gameTime;

      return (user = await User.findOneAndUpdate(
        {
          _id: currentUser._id,
          kids: { $elemMatch: { username: currentUser.username } }
        },
        {
          $addToSet: { "kids.$.points": { end, start, title, month, color } },
          $set: {
            "kids.$.gameTime": newGameTime
            // "kids.$.savedForTheMonth": moment().day("Friday") ? 0 : 1
          }
        }
      ));
    },

    useChildPoint: async (
      root,
      { password, pointsToUse },
      { currentUser, User }
    ) => {
      if (!currentUser) return null;
      parent = await User.findOne({ _id: currentUser._id });
      if (!parent) throw new Error("User not found");

      const isValidPassword = await bcrypt.compare(password, parent.password);
      if (!isValidPassword) throw new Error("Invalid password");
      // get kids old point
      const child = await User.findOne({
        _id: currentUser._id,
        kids: { $elemMatch: { username: currentUser.username } }
      });
      const currentKid = child["kids"].filter(
        el => el["username"] === currentUser.username
      );
      const result = currentKid[0].savedPoints;

      if (pointsToUse > result)
        throw new Error({
          msg: "Value required should not be larger than available value",
          type: "error"
        });
      //  Update child point
      return await User.findOneAndUpdate(
        {
          _id: currentUser._id,
          kids: { $elemMatch: { username: currentUser.username } }
        },
        {
          $set: {
            "kids.$.savedPoints": result - pointsToUse,
            "kids.$.savedForTheMonth": 0
          }
        },
        (err, result) => {
          if (result) {
            return { msg: "Points updated", type: "success" };
          } else {
            return { msg: "Sever error, try again", type: "error" };
          }
        }
      );
    },

    savePointTillNextMonthChildPoint: async (
      root,
      { password, bonusPoint },
      { currentUser, User }
    ) => {
      if (!currentUser) return null;
      parent = await User.findOne({ _id: currentUser._id });
      if (!parent) throw new Error("User not found");

      const isValidPassword = await bcrypt.compare(password, parent.password);
      if (!isValidPassword) throw new Error("Invalid password");
      // get kids old point
      const child = await User.findOne({
        _id: currentUser._id,
        kids: { $elemMatch: { username: currentUser.username } }
      });
      const currentKid = child["kids"].filter(
        el => el["username"] === currentUser.username
      );
      const result = currentKid[0].savedPoints;
      //  Update child point
      return await User.findOneAndUpdate(
        {
          _id: currentUser._id,
          kids: { $elemMatch: { username: currentUser.username } }
        },
        {
          $set: {
            "kids.$.savedPoints": result + bonusPoint,
            "kids.$.savedForTheMonth": 1
          }
        }
      );
    }
  }
};
