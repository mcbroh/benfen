const express = require('express');
const router = express.Router();

router.use('/math', require('./mathematics'));
router.use('/subjects', require('./subjects'));
router.use('/family', require('./family'));

module.exports = router;