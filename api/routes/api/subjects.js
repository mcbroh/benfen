const mongoose = require('mongoose');
const router = require('express').Router();
const Subjects = mongoose.model('Subjects');
const User = mongoose.model('User');
const { succesObj } = require('../../constants/Message');



router.get('/', async (req, res) => {

    const {_id, username} = req.currentUser;
    const serverSubjects = await Subjects.find({});

    User.findOne({_id}, (err, users) => {
        const currentKid = users['kids'].filter(el => el['username'] === username);
        let subjectsToClient =  [];

        if (currentKid.length > 0) { 
          const kidSubjects = currentKid[0]['subjects'] || [];     
          serverSubjects.forEach(subject => {       
            kidSubjects.forEach(singleSubject => {
              singleSubject['name'] === subject['serverName'] &&  subjectsToClient.push(subject)
            })
          })  
        }  else {
          subjectsToClient = serverSubjects;
        } 

        res.status(201).json({...succesObj, ...{ data: subjectsToClient }});  
      });
});

router.put('/my-subjects', async (req, res) => {

  const { _id } = req.currentUser;
  const { username, subject } = req.body;
  
  parent = await User.findOne({ _id });
  if (!parent) {
    res.json({ ...errorObj, ...{ message: 'User not found' } });
  } else {

    const currentKid = parent['kids'].map(el => {
      if (el['username'] === username) {
        el['subjects'] = setElement(el['subjects'])
        return el
      }
      return el
    });   

    user = await User.findOneAndUpdate({ _id }, { $set: { kids: currentKid } }, { new: true });
    res.status(201).json({...succesObj, ...{ message: 'Subject Added' }}); 
    
    function setElement(kidSubjects) {
      let exist = false;
      if (!subject['name']) return kidSubjects;
      if (!kidSubjects) return [subject];
      const updatedSubjects = kidSubjects.map(el => {
        if (subject['name'] === el['name']) {
          exist = true;
          return subject
        }
        return el
      })
      !exist && updatedSubjects.push(subject)      
      return updatedSubjects;
    } 

  }
});


module.exports = router;