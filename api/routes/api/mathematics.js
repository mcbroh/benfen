const mongoose = require('mongoose');
const router = require('express').Router();
const bcrypt = require('bcrypt');
const User = mongoose.model('User');
const MathQuestionGeneratorClass = require('../../controllers/math-question-generator')
const { succesObj, errorObj } = require('../../constants/Message');


/* GET api listing. */
router.get('/', async (req, res) => {
  const queryStrings = new Object;
  for (const key in req.query) {
    queryStrings[key] = req.query[key];
  }
  
  const { _id, username } = req.currentUser;
  const users = await User.findOne({ _id });
  const currentKid = users['kids'].filter(el => el['username'] === username);    
  const kidSubjects = currentKid[0]['subjects'];
  let testQueryVal = { // incase user does not have study
    hardnessMax: 5,
    hardnessMin: 2,
    amountOfQuestion: 10
  }
  kidSubjects.forEach(singleSubject => {
    singleSubject['name'] === queryStrings['subject'] && (testQueryVal = singleSubject);
  })

  const studyCont = new MathQuestionGeneratorClass(queryStrings['subject'], +testQueryVal['hardnessMin'], +testQueryVal['hardnessMax'])
  res.json({ ...succesObj, ...{ data: studyCont.getQuestion(testQueryVal['amountOfQuestion']) } });

});

router.put('/', async (req, res) => {

  const { _id, username } = req.currentUser;
  const vars = req.body;

  const child = await User.findOne({ _id, kids: { "$elemMatch": { username } } });
  if (!child) {
    res.json({ ...errorObj, ...{ message: 'User not found' } });
  } else {
    let timeGained = 0
    const currentKid = child['kids'].map(el => {
      if (el['username'] === username) {
        timeGained = (+el['gameTime'] || 0) + (+vars['timeGained']);

        const testResults = [];
        if ((el['testResults'] || []).length > 0) {
          el['testResults'].forEach(test => {
            if (test['subject'] !== vars['test']['subject']) {
              testResults.push(test)
            }
          })
        }
        testResults.push({ ...vars['test'] })
        el['gameTime'] = timeGained;
        el['testResults'] = testResults
        return el
      }
      return el
    });

    user = await User.findOneAndUpdate({ _id }, { $set: { kids: currentKid } }, { new: true });

    res.json({ ...succesObj, ...{ username, timeGained } });;
  }
});

router.put('/grant-time', async (req, res) => {
  const { _id, username } = req.currentUser;
  const vars = req.body;
  const parent = await User.findOne({ _id, kids: { "$elemMatch": { username } } });
  const isValidPassword = await bcrypt.compare(vars['password'], parent.password);
  if (!isValidPassword) {
    res.json({ ...errorObj, ...{ message: 'Wrong Password' } });
  } else {
    let goodToUpdate = true;
    const currentKid = parent['kids'].map(el => {
      if (el['username'] === username) {
        oldGameT = +el['gameTime']
        if (+vars['timeToUse'] > oldGameT) {
            goodToUpdate = false;
        } else el['gameTime'] = oldGameT - +vars['timeToUse'];
        return el
      }
      return el
    });
    if (goodToUpdate) {
      user = await User.findOneAndUpdate({ _id }, { $set: { kids: currentKid } }, { new: true });
      res.json(succesObj);
    } else res.json({ ...errorObj, ...{ message: 'Time asked for it more than time availble' } });
  }
});

router.put('/password-check', async (req, res) => {
  const { _id, username } = req.currentUser;
  const vars = req.body;
  const parent = await User.findOne({ _id, kids: { "$elemMatch": { username } } });
  const isValidPassword = await bcrypt.compare(vars['password'], parent.password);
  if (!isValidPassword) {
    res.json(errorObj);
  } else {
    res.json(succesObj);
  }
});


module.exports = router;