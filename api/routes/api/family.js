const mongoose = require('mongoose');
const router = require('express').Router();
const bcrypt = require('bcrypt');
const moment = require('moment');
const User = mongoose.model('User');

const { succesObj, errorObj } = require('../../constants/Message');

router.put('/', async (req, res) => {

  const { _id } = req.currentUser;
  const vars = req.body;
  const USER = await User.findOne({ _id });
  if (!USER) {
    res.json({ ...errorObj, ...{ message: 'User not found' } });
  } else {
    user = await User.findOneAndUpdate({ _id }, { $set: { 
      study: vars['study'], 
      dailyChores: vars['dailyChores'],
      bonusChores: vars['bonusChores'], 
    } }, { new: true });
    res.json({ ...succesObj, ...{  data: {
      study: user['study'], 
      dailyChores: user['dailyChores'],
      bonusChores: user['bonusChores']
    } } });
  }
});

router.put('/update-password', async (req, res) => {

  const { _id } = req.currentUser;
  j = null;
  
  const vars = req.body;
  const USER = await User.findOne({ _id });
  const isValidPassword = await bcrypt.compare(vars.password, USER.password);

  if (!isValidPassword) {
    !isValidPassword && res.json({ ...errorObj, ...{ message: 'Check password' } });
  } else {

    let hash = await bcrypt.hashSync(vars['newPassword'], bcrypt.genSaltSync(10));
    user = await User.findOneAndUpdate({ _id }, { $set: { password: hash } }, { new: true });
    res.json({ ...succesObj, data: { email: user['email'] } });
  }
});


router.put('/daily-chore', async (req, res) => {

  const { _id } = req.currentUser;
  const vars = req.body;
  const USER = await User.findOne({ _id });
  if (!USER) {
    res.json({ ...errorObj, ...{ message: 'User not found' } });
  } else {
    let choreAlreadyExist = false;
    const newDailyChoreData = USER['dailyChoreData'] || [];

    for (let index = 0; index < newDailyChoreData.length; index++) {
      if (newDailyChoreData[index]['id'] === vars['id']) {
        newDailyChoreData[index] = vars
        choreAlreadyExist = true;
        break;
      }
    }
    !choreAlreadyExist && newDailyChoreData.push(vars);

    user = await User.findOneAndUpdate({ _id }, { $set: { dailyChoreData: newDailyChoreData} }, { new: true });
    res.json({ ...succesObj, ...{  data: {
      dailyChoreData: user['dailyChoreData']
    } } });
  }
});

router.put('/extra-chore', async (req, res) => {

  const { _id } = req.currentUser;
  const vars = req.body;
  const USER = await User.findOne({ _id });
  if (!USER) {
    res.json({ ...errorObj, ...{ message: 'User not found' } });
  } else {
    const newExtraChore = USER.extraChore;
    newExtraChore.push(vars)
    user = await User.findOneAndUpdate({ _id }, { $set: { extraChore: newExtraChore} }, { new: true });
    res.json({ ...succesObj, ...{  data: {
      extraChore: user['extraChore']
    } } });
  }
});

router.put('/delete-extra-chore', async (req, res) => {

  const { _id } = req.currentUser;
  const vars = req.body;
  const USER = await User.findOne({ _id });
  if (!USER) {
    res.json({ ...errorObj, ...{ message: 'User not found' } });
  } else {
    const newExtraChore = USER.extraChore.filter(chore => chore['id'] !== vars['id']);
    user = await User.findOneAndUpdate({ _id }, { $set: { extraChore: newExtraChore} }, { new: true });
    res.json({ ...succesObj, ...{  data: {
      extraChore: user['extraChore']
    } } });
  }
});

router.put('/update-child', async (req, res) => {

  const { _id, username } = req.currentUser;
  
  const originalVars = req.body;
  const id = moment().format("YYYYMMDD")+originalVars.type;
  const USER = await User.findOne({ _id, kids: { "$elemMatch": { username } } });
  const isValidPassword = await bcrypt.compare(originalVars.password, USER.password);

  if (!USER || !isValidPassword) {
    !USER && res.json({ ...errorObj, ...{ message: 'User not found' } });
    !isValidPassword && res.json({ ...errorObj, ...{ message: 'Check password' } });
  } else {
    // allowed vars in variables passed
    const variables = {...originalVars};
    delete variables.password;
    const allowedVars = ['gameTime', 'pass', 'savedPoints', 'useGameTime', 'type' ];
    const vars = {};
    const variablesKeys = Object.keys(variables);
    let goodToUpdate = {update: true, type: null};
    
    for (let index = 0; index < variablesKeys.length; index++) {
      const element = variablesKeys[index];
      if (allowedVars.includes(element) ) {
        vars[element] = variables[element];
      } else {
        res.json({ ...errorObj, message: 'Variable submited is not included' });
        break;
      }
    }

    const currentKid = USER['kids'].map(kid => {
      if (kid['username'] === username) {
        Object.keys(vars).forEach(_var => {          
          switch (_var) {
            case 'pass':
              kid[_var] = vars[_var]
              break;
            case 'gameTime':
            case 'savedPoints':
              kid[_var] = (+kid[_var] || 0) + (+vars[_var]);
              break;
            case 'type':
            console.log({kid});

              if ( !kid['choresDone'] )  kid['choresDone'] = [id];
              else if (!kid['choresDone'].includes(id)) {
                kid['choresDone'].length === 30 && kid['choresDone'].shift();
                kid['choresDone'].push(id);
              } 
              else goodToUpdate = {update: false, type: 'choresDone'};
              break;
            case 'useGameTime':
              if ( (+kid['gameTime']) > (+vars['gameTime']) ) kid['gameTime'] = (+kid['gameTime']) - (+vars['gameTime']);
              else goodToUpdate = {update: false, type: 'gameTime'};
              break;
          }
        })
        return kid
      }
      return kid
    });
    if (goodToUpdate.update) {
      // console.log(currentKid);
      user = await User.findOneAndUpdate({ _id }, { $set: { kids: currentKid } }, { new: true });
      res.json({ ...succesObj, data: { kids: user['kids'] } });
    } else {
      goodToUpdate.type === 'gameTime' && res.json({ ...errorObj, message: 'You dont have enough time to use' });
      goodToUpdate.type === 'choresDone' && res.json({ ...errorObj, message: 'Chores already performed today' });
    }
  }
});

router.put('/extra-chore-done', async (req, res) => {

  const { _id, username } = req.currentUser;
  
  const vars = req.body;
  const USER = await User.findOne({ _id, kids: { "$elemMatch": { username } } });
  const isValidPassword = await bcrypt.compare(vars.password, USER.password);

  if (!USER || !isValidPassword) {
    !USER && res.json({ ...errorObj, ...{ message: 'User not found' } });
    !isValidPassword && res.json({ ...errorObj, ...{ message: 'Check password' } });
  } else {    
   const currentKid = USER['kids'].map(kid => {
      if (kid['username'].toLowerCase() === username.toLowerCase()) {
        vars['doneChores'].forEach(id => {
          USER['extraChore'].forEach(chore => {
              if(chore['id'] === id) {
                  if (chore['rewardAmount'] > 0 ) kid['savedPoints'] = (isNaN(kid['savedPoints']) ? 0 : +kid['savedPoints']) + (+chore['rewardAmount']);
                  if (chore['rewardMinute'] > 0 ) kid['gameTime'] = (isNaN(kid['gameTime']) ? 0 : +kid['gameTime']) + (+chore['rewardMinute']);
              }
          })
        })
        return kid
      }
      return kid
    });    
    if (!choreAlreadyDone(USER['extraChore'], vars['doneChores'])) {
      user = await User.findOneAndUpdate({ _id }, { $set: { 
        kids: currentKid,  extraChore: newExtraChorArr(USER['extraChore'], vars['doneChores'])
      } }, { new: true });
      res.json({ ...succesObj, data: { kids: user['kids'], extraChore: user['extraChore'] } }); 
    } else {
      res.json({ ...errorObj, message: 'Chores already performed today' });
    } 
  }
});

router.put('/update-extra-chore', async (req, res) => {

  const { _id } = req.currentUser;
  
  const vars = req.body;
  const USER = await User.findOne({ _id });

  if (!USER) res.json({ ...errorObj, ...{ message: 'User not found' } });
  else {  
    console.log({vars});
    user = await User.findOneAndUpdate({ _id }, { 
      $set: { extraChore: updateExtraChore(USER['extraChore'], vars) } 
    }, { new: true });
    res.json({ ...succesObj, ...{  data: { extraChore: user['extraChore'] } } });
  }
});

router.put('/add-chore-to-kid', async (req, res) => {

  const { _id } = req.currentUser;
  
  const vars = req.body;
  const USER = await User.findOne({ _id });  

  if (!USER) {
    !USER && res.json({ ...errorObj, ...{ message: 'User not found' } });
  } else {
    const currentKid = USER['kids'].map(kid => {
      if (vars['users'].includes(kid['username'])) {
        if (kid['dailyChores']) kid['dailyChores'].push(vars['choreId'])
        else kid['dailyChores'] = [vars['choreId']]
      } else if (kid['dailyChores'] && kid['dailyChores'].includes(vars['choreId'])) { 
          kid['dailyChores'] = kid['dailyChores'].filter(chore =>  chore !== vars['choreId'])
      }
      return kid
    });    
     user = await User.findOneAndUpdate({ _id }, { $set: { kids: currentKid } }, { new: true });
      res.json({ ...succesObj, data: { kids: user['kids'] } }); 
  }
});

updateExtraChore = (originalChores, updatedChore) => {
  const retval = originalChores.map(chore => {
    if (chore['id'] === updatedChore['id']) return {...chore, done: updatedChore['state']};
    return chore;
  })
  return retval;
}

choreAlreadyDone = (originalChores, incommingIds) => {
  let retval = false;
  incommingIds.forEach(id => {
    originalChores.forEach(chore => {
      if (chore['id'] === id) retval = chore['done'];
    })
  })
  return retval
}

newExtraChorArr = (originalChores, incommingIds) => {
  incommingIds.forEach(id => {
    originalChores.forEach(chore => {
      if (chore['id'] === id) chore['done'] = true;
    })
  })  
  return originalChores
}


module.exports = router;