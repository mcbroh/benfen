module.exports = {
    succesObj: {
        data: null,
        status: 'success',
        success: true,
        message: null
    },
    errorObj: {
        data: null,
        status: 'error',
        success: false,
        message: null 
    }, 
    gqlSuccesObj: {
      token: null, 
      type: null, 
      message: null,
      success: true
    },
    gqlErrorObj: {
      token: null, 
      type: null, 
      message: null,
      success: false
    }
}