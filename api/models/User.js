const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const basic = {
  type: String,
  required: true
};

const basicObject = {
  used: { type: Boolean, default: true },
  useGameTime: Boolean,
  useMoney: Boolean,
  point: Number,
  rewardMinute: Number,
  rewardAmount: Number
};

const subjectType = {
  name: String,
  levelName: String,
  hardnessMin: String,
  hardnessMax: String,
  timeToGain: String,
  amountOfQuestion: String
};
const kidType = {
  choresDone: [],
  dailyChores: [],
  subjects: [],
  gameTime: { type: Number, default: 0 },
  name: { ...basic },
  pass: String,
  savedPoints: { type: Number, default: 0 },
  username: { ...basic }
};

const UserSchema = new Schema({
  avatar: String,
  familyName: { ...basic, unique: true },
  password: basic,
  email: { ...basic, unique: true },
  messages: [],
  kids: [kidType],
  houserules: [],
  rewardDay:
    "Monday" |
    "Tuesday" |
    "Wednessday" |
    "Thursday" |
    "Friday" |
    "Saturday" |
    "Sunday",
  dailyChoreData: [],
  choreTodo: [],
  extraChore: [],
  familyTime: [],
  rewards: [],
  study: basicObject,
  dailyChores: basicObject,
  bonusChores: basicObject,
  subjects: [subjectType],
  joinDate: { type: Date, default: Date.now }
});

UserSchema.pre("save", function(next) {
  if (!this.isModified("password")) return next();

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);

      this.password = hash;

      next();
    });
  });
});

module.exports = mongoose.model("User", UserSchema);
