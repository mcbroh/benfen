const mongoose = require('mongoose');

const { Schema } = mongoose;
const basic = {
  type: String,
  required: true
}

const SubjectsSchema = new Schema({
  link: basic,
  name: basic,
  levelName: basic,
  serverName: basic
});


mongoose.model('Subjects', SubjectsSchema);

