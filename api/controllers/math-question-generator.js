class MathQuestionGeneratorClass {

  constructor(queryString, lowestNumber, highestNumber) {
    this.queryString = queryString;
    this.lowestNumber = lowestNumber + 1;
    this.highestNumber = highestNumber + 1;    
    
    this.operators = {
      addition:  {
        sign: "+",
        method: function (x, y) { return x + y; }
      }, 
      subtraction: {
        sign: "-",
        method: function (x, y) { return x - y; }
      }, 
      multiplication: {
        sign: "*",
        method: function (x, y) { return x * y; }
      }, 
      division: {
        sign: "/",
        method: function (x, y) { return x / y; }
      }
    };
  }

  getQuestion(numOfQuestionsToReturn) {
    
    const generatedQuestions = [];
    let numOfCreatedQuestions = 0;
    const mathematicalOperator = this.operators[this.queryString];

    while (numOfCreatedQuestions < numOfQuestionsToReturn) {
      
      let x = Math.floor(Math.random() * this.highestNumber), y = Math.floor(Math.random() * this.highestNumber);
      if (mathematicalOperator['sign'] === '-') {
        while (y < this.lowestNumber) { y = Math.floor(Math.random() * this.highestNumber) }
        while (x < y) { y = Math.floor(Math.random() * this.highestNumber) }
      }

      if (mathematicalOperator['sign'] === '+') {
        while (y < this.lowestNumber) { y = Math.floor(Math.random() * this.highestNumber) }
        while (x < this.lowestNumber) { x = Math.floor(Math.random() * this.highestNumber) }
      }

      if (mathematicalOperator['sign'] === '*') {
        x = Math.floor(Math.random() * this.lowestNumber); y = Math.floor(Math.random() * this.highestNumber);
        while (x < 2) { x = Math.floor(Math.random() * this.lowestNumber) }
      }

      if (mathematicalOperator['sign'] === '/') {
        while (this.lowestNumber > y || this.lowestNumber > x || x % y > 0 || y === x || y == 0 || y == 1 ) {
          x = Math.floor(Math.random() * this.highestNumber); y = Math.floor(Math.random() * this.highestNumber);
        } 
      }
      generatedQuestions.push({
        sign: mathematicalOperator['sign'], 
        // testAnswere: mathematicalOperator.method(x, y),
        x,
        y,
      });
      numOfCreatedQuestions += 1;
    }
    return generatedQuestions;
  }
}

module.exports = MathQuestionGeneratorClass

