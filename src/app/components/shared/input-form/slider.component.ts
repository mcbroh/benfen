import { Component, OnInit, forwardRef, Input, Injector, AfterContentInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';

@Component({
  selector: 'app-slider-form',
  templateUrl: './slider.component.html',
  styleUrls: ['./input-form.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SliderFormComponent),
      multi: true,
    }
  ]
})
export class SliderFormComponent implements ControlValueAccessor, OnInit, AfterContentInit {
  
  @Input() min: number = 0;
  @Input() max: number = 100;
  @Input() label: string;
  @Input() valueType: string = '';

  public value: number = 0;
  public valuePlusHolder;
  public control;
  
  onChange: (_: any) => void;
  onTouched: () => void;
  disabled: boolean;

  ngControl: NgControl;

  constructor(
    private inj: Injector
  ) { }

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl);
    this.control = this.ngControl;    
  }

  ngAfterContentInit() {
    this.control = this.ngControl.control;
    this.setValuePlusHolder();
  }

  setValuePlusHolder(): void {
    this.valuePlusHolder = `${this.value} ${this.valueType}`
  }

  pushChanges(value: any) {
    this.onChange(value);
    this.setValuePlusHolder();
  }

  writeValue(value: any): void {    
    this.value = value || 0;
    this.setValuePlusHolder();
  }
  registerOnChange(fn: any): void { 
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }


}
