import { Component, OnInit, forwardRef, Input, Injector, AfterContentInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { BehaviorSubject, Subscription } from 'rxjs';

import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputFormComponent),
      multi: true,
    },
    LanguageService
  ]
})
export class InputFormComponent implements ControlValueAccessor, OnInit, AfterContentInit {
  control;
  @Input() min: number;
  @Input() max: number;
  @Input() mask: boolean;
  @Input() hasError: boolean;
  @Input() registerMode: boolean;
  @Input() floatLabel: boolean = true;
  @Input() label: string;
  @Input() hint: string;
  @Input() type: string = 'text';
  @Input() pattern: string = 'numbers';
  @Input() keyFilter: 'pint' | 'int' | 'pnum' | 'num' | 'hex' | 'email' | 'alpha' | 'alphanum';

  value: any;
  onChange: (_: any) => void;
  onTouched: () => void;
  disabled: boolean;

  ngControl: NgControl;

  langSub: Subscription;
  lang = new BehaviorSubject<any>({});

  constructor(
    private inj: Injector,
    private langService: LanguageService
  ) { 
    this.langSub = this.langService.selectedLang.subscribe(lang => { this.lang.next(lang['auth']) })
  }

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl);
    this.control = this.ngControl;    
  }

  ngAfterContentInit() {
    this.control = this.ngControl.control;
  }

  writeValue(value: any): void {    
    this.value = value;
  }
  registerOnChange(fn: any): void { 
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }


}
