import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { LanguageService } from 'src/app/service/language.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-empty-state',
  templateUrl: './empty-state.component.html',
  styleUrls: ['./empty-state.component.scss']
})
export class EmptyStateComponent implements OnInit, OnDestroy {

  @Input() msgHead: string = '';
  @Input() msgBody: string = '';
  @Input() position: string = '';
  @Input() start: boolean = false;

  langSub: Subscription;
  lang: object;
  
  constructor( 
    private langService: LanguageService
  ) { 
    this.langSub = this.langService.selectedLang.subscribe(lang => { this.lang = lang['emptyState'] })
  }
  
  ngOnInit() { 
    switch (this.position) {
      case 'right':
        this.msgBody = this.msgBody + ' ' + this.lang['right']
        break;
      case 'left':
        this.msgBody = this.msgBody + ' ' + this.lang['left']
        break;
      case 'topRight':
        this.msgBody = this.msgBody + ' ' + this.lang['topRight']
        break;
      case 'topLeft':
        this.msgBody = this.msgBody + ' ' + this.lang['topLeft']
        break;
      case 'kidCard':
        this.msgBody = this.lang['kidCard']
        break;
      default:
        break;
    }

    if (this.start) {
      this.msgBody = this.lang['start'] + ' ' + this.msgBody.toLowerCase();
    }
  }

  ngOnDestroy() {
    this.langSub.unsubscribe();
  }

}

