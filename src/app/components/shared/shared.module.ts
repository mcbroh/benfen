import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {InputTextModule} from 'primeng/inputtext';
import {MessageModule} from 'primeng/message';
import {TooltipModule} from 'primeng/tooltip';
import {ButtonModule} from 'primeng/button';
import {PasswordModule} from 'primeng/password';
import {SliderModule} from 'primeng/slider';
import {KeyFilterModule} from 'primeng/keyfilter';
import {InputMaskModule} from 'primeng/inputmask';


import {MatDialogModule} from '@angular/material/dialog';

import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component'

import { UrlSafePipe } from './pipes/url-safe.pipe';
import { InputFormComponent } from './input-form/input-form.component';
import { SliderFormComponent } from './input-form/slider.component';
import { ButtonComponent } from './button/button.component';
import { EmptyStateComponent } from './empty-state/empty-state.component';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    FormsModule,
    TooltipModule,
    SliderModule,
    MessageModule,
    ButtonModule,
    PasswordModule,
    InputTextModule,
    InputMaskModule,
    KeyFilterModule
  ],
  declarations: [
    UrlSafePipe,
    InputFormComponent,
    SliderFormComponent,
    ConfirmDialogComponent,
    ButtonComponent,
    EmptyStateComponent,
  ],
  exports: [
    UrlSafePipe,
    InputFormComponent,
    SliderFormComponent,
    ButtonComponent,
    EmptyStateComponent
  ],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class SharedModule { }
