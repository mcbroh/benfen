import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient, HttpHeaders } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { Apollo } from 'apollo-angular';

import { GET_CURRENT_USER } from '../../../Apollo'
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  token: string;

  constructor(
    private angularApollo: Apollo,
    private httpClient: HttpClient,
    private messageService: MessageService
  ) {
    this.token = localStorage.getItem('token');
  }

  httpGet(address) {
    return this.httpClient.get(`/api/${address}`, { headers: new HttpHeaders().set('Auth', this.token) })
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  httpUpdate(address, variables: any): Observable<any> {
    return this.httpClient.put(
      `/api/${address}`,
      variables,
      { headers: new HttpHeaders().set('Auth', this.token) }
    ).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }


  public updateApolloCache(data: object) {
    // Read the data from our cache for this query.
    const storedDataByQuery = this.angularApollo.getClient().readQuery({
      query: GET_CURRENT_USER
    });
    Object.keys(data).forEach(key => {
      if (key !== 'kids') {
        if (Array.isArray(data[key])) {
          const newArrData = []
          data[key].forEach((obj, index) => {
            /* storedDataByQuery['getCurrentUser'][key][index] = { ...storedDataByQuery['getCurrentUser'][key][0], ...obj }; */
            newArrData.push({...obj, __typename: storedDataByQuery['getCurrentUser'][key][0]['__typename']})
          })
          storedDataByQuery['getCurrentUser'][key] = newArrData;
        } else storedDataByQuery['getCurrentUser'][key] = { ...storedDataByQuery['getCurrentUser'][key], ...data[key] };
      } else {
        data[key].forEach((kid, index) => {
          const newObj = {}
          const allowedVars = ['gameTime', 'pass', 'savedPoints', 'choresDone', 'extraChore', 'dailyChores']

          allowedVars.forEach(keyInKid => { newObj[keyInKid] = kid[keyInKid] })
          storedDataByQuery['getCurrentUser'][key][index] = { ...storedDataByQuery['getCurrentUser'][key][index], ...newObj };
        });
      }

    })    

    // Write our data back to the cache.
    this.angularApollo.getClient().writeQuery({
      query: GET_CURRENT_USER,
      data: storedDataByQuery
    });
    this.messageService.add({severity: 'success', summary: 'Hurray!!!'});
  }



  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
