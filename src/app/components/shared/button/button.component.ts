import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() icon: string = null;
  @Input() label: string = 'Add label';
  @Input() disabled: boolean = false;
  @Input() working: boolean = false;
  @Input() type: string;
  @Output() onClick = new EventEmitter<any>();

  btnIcon: string;
  btnClass: string;

  ngOnInit() {
    this.setBtnClassAndIcon()
  }

  onClickHandler() {
    !this.disabled && this.onClick.emit(true);
  }

  setBtnClassAndIcon(): void {
    switch (this.type) {
      case 'create':
        this.btnClass = '';
        this.btnIcon = this.icon || 'pi pi-plus';
        break;
      case 'secondary':
        this.btnClass = "ui-button-secondary";
        this.btnIcon = this.icon || '';
        break;
      case 'success':
        this.btnClass = "ui-button-success";
        this.btnIcon = this.icon || '';
        break;
      case 'info':
        this.btnClass = "ui-button-info";
        this.btnIcon = this.icon || '';
        break;
      case 'warning':
        this.btnClass = "ui-button-warning";
        this.btnIcon = this.icon || '';
        break;
      case 'delete':
        this.btnClass = "ui-button-danger";
        this.btnIcon = this.icon || 'pi pi-trash';
        break;
      default:
        this.btnClass = '';
        this.btnIcon = this.icon || null;
    }
  }


}
