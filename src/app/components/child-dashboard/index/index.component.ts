import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Apollo } from 'apollo-angular-boost';
import { Subscription, BehaviorSubject } from 'rxjs';
import { CalendarEvent } from 'angular-calendar';
import { startOfDay, endOfDay } from 'date-fns';
import * as moment from 'moment';

import { GET_CURRENT_USER } from '../../../Apollo';
import { MutationService } from '../../../service/mutation.service'
import { LanguageService } from 'src/app/service/language.service';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-child-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexComponent implements OnInit, OnDestroy {

  events: CalendarEvent[] = [];
  openedTab = {};
  step: number = 0;
  me: object = { name: '' };
  savedPoints: number;
  
  data;
  currMonthName: String;
  dateOfBirth: any;

  houseRulePanel: boolean;

  langSub: Subscription;
  querySub: Subscription;

  loading = new BehaviorSubject(true);
  lang = new BehaviorSubject<object>({});

  constructor(
    private apollo: Apollo,
    private mutator: MutationService,
    private langService: LanguageService,
  ) { 
    this.langSub = this.langService.selectedLang.subscribe(lang => { 
      this.lang.next(lang['childModule']) 
    })
  }

  ngOnInit() {
    this.getCurrentUserDtails();
    this.currMonthName = moment().format('MMMM');
  }

  addEvent({type, password, gameTime}): void {
    const mutationDetails = {
      title: type,
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors[type === 'morning' ? 'blue' : 'yellow'],
      password,
      gameTime,
      month: this.currMonthName
    };
    this.mutator.actionMutate(
      'ADD_POINT',
      mutationDetails,
      'Point added',
      [{ query: 'GET_CURRENT_USER' }]
    ).subscribe(() => {
      this.step = 0;
      this.events.push(mutationDetails);
    });
  }

  ngOnDestroy() {
    this.querySub.unsubscribe();
    this.langSub.unsubscribe();
  }

  getCurrentUserDtails() {
    const username = localStorage.getItem('username');
    this.querySub = this.apollo.watchQuery({
      query: GET_CURRENT_USER,
      fetchPolicy: 'cache-first'
    }).valueChanges.subscribe(({ data, loading }) => {      
      !loading && (this.data = data['getCurrentUser']);
      !loading && (
        this.data['kids'].forEach(kid => {
          // Compare name
          if (kid['username'].toLowerCase() === username.toLowerCase()) {
            typeof kid['savedPoints'] !== 'undefined' && (this.savedPoints = kid['savedPoints']);
            this.me['name'] = kid['name'] + '  ' + this.data['familyName'] + '!';
            this.dateOfBirth = kid['dateOfBirth'] ? kid['dateOfBirth'] : null;
            kid['dateOfBirth'] && this.checkBirthDay(kid['dateOfBirth'])
            kid['points'] && this.setEvents(kid['points']);
            this.loading.next(false);
          }
        })
      );
    });
  };

  setEvents(rawpoints: Array<any>): void {
    this.events = rawpoints.map(obj => ({
      ...obj,
      ...{ start: new Date(obj['start']), end: new Date(obj['end']) },
      color: obj['color'][0]
    }))
  }

  checkBirthDay(dob) {
    if (
      moment(dob).get('date') === moment().get('date') &&
      moment(dob).get('month') === moment().get('month')
    ) {

      console.log(moment().get('year') + '-' + moment(dob).get('year'));
      console.log('You are: ' + (+moment().get('year') - +moment(dob).get('year')) + ' yrs old today');
      console.log('hurra hurra');
    }

  }
}
