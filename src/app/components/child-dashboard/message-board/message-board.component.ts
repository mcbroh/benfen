import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { GET_CURRENT_USER } from "../../../Apollo";
import { Apollo } from "apollo-angular";
import { Subscription, BehaviorSubject } from "rxjs";
import { Router } from "@angular/router";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { MutationService } from "src/app/service/mutation.service";
import { LanguageService } from "src/app/service/language.service";
import { DataService } from "../../shared/service/data.service";
import { MessageService } from "primeng/api";
import * as moment from "moment";

interface ExtraChoreConfig {
  id: string;
  done: string;
  todo: string;
  points: string;
}

interface FamilyTimeConfig {
  id: string;
  day: string;
  time: string;
  todo: string;
}

@Component({
  selector: "app-message-board",
  templateUrl: "./message-board.component.html",
  styleUrls: ["./message-board.component.scss"]
})
export class MessageBoardComponent implements OnInit, OnDestroy {
  extraChores: ExtraChoreConfig[];
  familyTime: FamilyTimeConfig[];

  form: FormGroup;
  mutationType: string = "save";

  langSub: Subscription;
  querySub: Subscription;

  lang = new BehaviorSubject<object>({});
  showForm = new BehaviorSubject<boolean>(false);
  showWeeklyPayout = new BehaviorSubject<boolean>(false);
  processing = new BehaviorSubject<boolean>(false);
  useExtraChores: boolean;
  useStudy: boolean;
  point = 0;
  gameTime = 0;
  doneChores = [];
  extraChoreObj: any;
  password: string = "";

  constructor(
    private apollo: Apollo,
    private router: Router,
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private mutator: MutationService,
    private langService: LanguageService,
    private messageService: MessageService
  ) {
    this.createForm();
    this.langSub = this.langService.selectedLang.subscribe(lang => {
      this.lang.next(lang["childModule"]);
    });
  }

  ngOnInit() {
    this.getMyGameTime();
  }

  createForm() {
    this.form = this.formBuilder.group(
      {
        amount: [0, Validators.required],
        password: ["", Validators.required]
      },
      { updateOn: "blur" }
    );
    this.form.controls["amount"]["label"] = "Reward Bonus";
    this.form.controls["password"]["label"] = "Parent's password";
  }

  changeMode(placeHolder, mutationType) {
    this.mutationType = mutationType;
    this.form.controls["amount"]["label"] = placeHolder;
  }

  getMyGameTime = async () => {
    const username = await localStorage.getItem("username");
    this.querySub = this.apollo
      .watchQuery({
        query: GET_CURRENT_USER,
        fetchPolicy: "cache-first"
      })
      .valueChanges.subscribe(({ data }) => {
        if (data) {
          this.extraChores = data["getCurrentUser"]["extraChore"].filter(
            chore => !chore["done"]
          );
          this.familyTime = data["getCurrentUser"]["familyTime"];
          this.useExtraChores =
            data["getCurrentUser"]["bonusChores"]["used"] || false;
          this.extraChoreObj = data["getCurrentUser"]["bonusChores"];
          data["getCurrentUser"]["kids"].forEach(kid => {
            if (kid["username"].toLowerCase() === username.toLowerCase()) {
              this.useStudy =
                (data["getCurrentUser"]["study"]["used"] &&
                  kid["subjects"].length > 0) ||
                false;
              const weeklyPaymentMade = !!kid["savedForTheMonth"];
              if (
                !weeklyPaymentMade &&
                moment().day(data["getCurrentUser"]["rewardDay"])
              )
                this.showWeeklyPayout.next(true);
            }
          });
        }
      });
  };

  mutateToServer() {
    if (this.form.valid) {
      const vars = this.form.value;
      this.processing.next(true);

      const data = {
        save: {
          mutationName: "SAVE_KID_POINT_WITH_BONUS",
          vars: { bonusPoint: vars.amount, password: vars.password },
          msg: "Bonus Added"
        },
        use: {
          mutationName: "CASH_OUT_KID_POINTS",
          vars: { pointsToUse: vars.amount, password: vars.password },
          msg: "Point Deducted"
        }
      };
      this.mutator
        .actionMutate(
          data[this.mutationType]["mutationName"],
          data[this.mutationType]["vars"],
          data[this.mutationType]["msg"],
          [{ query: "GET_CURRENT_USER" }]
        )
        .subscribe(() => {
          // this.placeHolder = null
          this.processing.next(false);
        });
    }
  }

  routeToStudy() {
    this.router.navigateByUrl("/study");
  }

  showInput = () => this.doneChores.length > 0;

  displayReward(event, chore) {
    this.point =
      this.point +
      (event === true ? +chore["rewardAmount"] : -+chore["rewardAmount"]);
    this.gameTime =
      this.gameTime +
      (event === true ? +chore["rewardMinute"] : -+chore["rewardMinute"]);
    event === true
      ? this.doneChores.push(chore["id"])
      : (this.doneChores = this.doneChores.filter(x => x !== chore["id"]));
  }

  public triggerSetReward() {
    this.processing.next(true);
    this.dataService
      .httpUpdate("family/extra-chore-done", {
        doneChores: this.doneChores,
        password: this.password
      })
      .subscribe(res => {
        if (res.success) {
          this.point = 0;
          this.gameTime = 0;
          this.doneChores = [];
          this.password = "";
          this.dataService.updateApolloCache(res["data"]);
        } else {
          this.messageService.add({
            severity: "error",
            summary: "Error",
            detail: res.message
          });
        }
        this.processing.next(false);
      });
  }

  ngOnDestroy() {
    this.querySub.unsubscribe();
    this.langSub.unsubscribe();
  }
}
