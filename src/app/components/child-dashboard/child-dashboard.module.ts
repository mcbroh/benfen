import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';

import {MatExpansionModule} from '@angular/material/expansion';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';

import {FieldsetModule} from 'primeng/fieldset';
import {ButtonModule} from 'primeng/button';
import {CheckboxModule} from 'primeng/checkbox';
import {ProgressBarModule} from 'primeng/progressbar';

import { ChildRoutingModule } from './child-routing.module';
import { SharedModule } from '../shared/shared.module';

import { IndexComponent } from './index/index.component';
import {RulesComponent} from './rules/rules.component';
import { ChoresComponent } from './chores/chores.component';
import { CalenderComponent } from './calender/calender.component';
import { MessageBoardComponent } from './message-board/message-board.component'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ChildRoutingModule,
    
     // Material
     MatExpansionModule,
     MatSelectModule,
     MatIconModule,
     MatTableModule,
     MatButtonModule,
     MatCardModule,
     SharedModule,
     MatListModule,

     // Primeng
     FieldsetModule,
     ButtonModule,
     CheckboxModule,
     ProgressBarModule,
     
     // others
     CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  declarations: [
    IndexComponent,
    RulesComponent,
    ChoresComponent,
    CalenderComponent,
    MessageBoardComponent
  ]
})
export class ChildDashboardModule { }
