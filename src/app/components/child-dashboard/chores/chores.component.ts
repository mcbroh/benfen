import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, ChangeDetectorRef, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { Apollo } from 'apollo-angular';
import { CalendarEvent } from 'angular-calendar';
import { BehaviorSubject, Subscription } from 'rxjs';
import { MessageService } from 'primeng/api';

import { nowIsBefore, nowIsAfter, isCorrectUsername } from '../../../_helpers/index';
import { GET_CURRENT_USER } from '../../../Apollo';
import { LanguageService } from 'src/app/service/language.service';
import { DataService } from '../../shared/service/data.service';


const now = moment(); 

interface  DailyChoreObj {
  point: number;
  rewardAmount: number;
  rewardMinute: number;
  useGameTime: boolean;
  useMoney: boolean;
  used: boolean;
}

@Component({
  selector: 'app-child-chores',
  templateUrl: './chores.component.html',
  styleUrls: ['./chores.component.scss']
})
export class ChoresComponent implements OnInit , OnChanges, OnDestroy {
  
  @Input() events: CalendarEvent[] = [];
  @Output() setReward = new EventEmitter();

  chores = [];
  value = 0;
  option;
  optionSet = false;

  password = '';
  amount = 0;
  useDailyChores: DailyChoreObj;

  langSub: Subscription;
  querySub: Subscription;

  dropDownOption: Array<object> = []
  lang = new BehaviorSubject<object>({});
  todo = new BehaviorSubject<object>({});
  choreDone = new BehaviorSubject<boolean>(false);
  processing = new BehaviorSubject<boolean>(false);
  roundVal: number = 0;
  
  constructor(
    private apollo: Apollo,
    private dataService: DataService,
    private langService: LanguageService,
    private messageService: MessageService,
    private cdr: ChangeDetectorRef
  ) {
    this.langSub = this.langService.selectedLang.subscribe(lang => { 
      this.lang.next(lang['childModule']) 
    })
   }

  ngOnInit() {   
    this.getChores();  
  }

   ngOnChanges(changes: SimpleChanges): void {
    if (changes['events']) {
      this.setCHores();
    }
  }

  getChores() {
    const username = localStorage.getItem('username');
    this.querySub = this.apollo.watchQuery({
      query: GET_CURRENT_USER,
      fetchPolicy: 'cache-first'
    }).valueChanges.subscribe(({ data }) => {            
      if (data) { 
        this.amount = data['getCurrentUser']['dailyChores']['rewardMinute'] || 0;
        this.useDailyChores = <DailyChoreObj>data['getCurrentUser']['dailyChores'];
        const me = data['getCurrentUser']['kids'].filter(kid => kid['username'] === username)[0];
        console.log(me);
        
        const myChore = [];
        me.dailyChores.forEach(chore => {
          myChore.push(data['getCurrentUser']['dailyChoreData'].filter(x => x['id'] === chore)[0]) 
        })
        
        myChore.length > 0 && this.getMyChores(myChore, me['choresDone']);
        this.setCHores();
      }
    });
  };

  getMyChores(allChores: Array<any>, doneChores: Array<string>): void {    
    this.chores = allChores.map(chore => ({
        ...chore,
        checked: doneChores ? doneChores.includes(moment().format("YYYYMMDD") + chore['id']) : false
    }));    
  }
  
  setCHores() {
    let todo = {};
    this.chores.forEach(chore => { 
      if ( nowIsBefore(chore['choreClose']) && nowIsAfter(chore['choreOpen']) ) {
        !this.optionSet && (this.option = chore['id']);
        this.optionSet = true;
        todo = chore
      }
    })
    this.todo.next(todo)     
    this.cdr.markForCheck();     
  }

  public choreChecked(checked) {
    const number = 100 / this.todo['value']['chores'].length
    if (checked) this.value = this.value + number;
    else this.value = this.value - number

    this.roundVal = Math.round(this.value)
  }

  public triggerSetReward() {  
    this.processing.next(true);  
    const dataToMutate = {
      password: this.password,
      gameTime: this.amount,
      savedPoints: this.useDailyChores.rewardAmount,
      type: this.option
    }
    this.dataService.httpUpdate('family/update-child', dataToMutate).subscribe(res => {
      if (res.success) {
        this.value = 0;
        this.password = '';
        this.dataService.updateApolloCache(res['data'])
      } else {
        this.messageService.add({severity: 'error', summary: 'Error', detail: res.message});
      }
      this.processing.next(false);
    });
  }

  ngOnDestroy() {
    this.langSub.unsubscribe();
    this.querySub.unsubscribe();
  }

}
