import { Component, OnInit } from '@angular/core';
import { LanguageService } from 'src/app/service/language.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { DataService } from '../../shared/service/data.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  form: FormGroup;
  langSub: Subscription;
  lang = new BehaviorSubject<any>({});
  processing = new BehaviorSubject(false);
  
  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private config: DynamicDialogConfig,
    private dialogRef: DynamicDialogRef,
    private langService: LanguageService,
  ) { 
    this.langSub = this.langService.selectedLang.subscribe(lang => { this.lang.next(lang['auth']) })
    this.config.header = this.lang.value.resetPass;
    this.initForm();
  }

  ngOnInit() { 
    
  }

  initForm() {
    this.form = this.formBuilder.group({
      password: ['', Validators.required],
      newPassword: ['', Validators.required]
    }, { updateOn: 'blur' });
  }

  actionCreate() {
    this.processing.next(true);
    const dataToMutate = this.form.value;
    this.dataService.httpUpdate('family/update-password', dataToMutate).subscribe(res => {
      if (res.success) {  
        this.dataService.updateApolloCache(res['data']);
        this.dialogRef.close()
      }
    });  
  }

}
