import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ParentRoutingModule } from './parent-routing.module';
import { SharedModule } from '../shared/shared.module';

import {FieldsetModule} from 'primeng/fieldset';
import {CalendarModule} from 'primeng/calendar';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextModule} from 'primeng/inputtext';
import {SliderModule} from 'primeng/slider';
import {DialogModule} from 'primeng/dialog';
import {CardModule} from 'primeng/card';
import {TableModule} from 'primeng/table';
import {PanelModule} from 'primeng/panel';
import {ButtonModule} from 'primeng/button';
import {AccordionModule} from 'primeng/accordion';
import {KeyFilterModule} from 'primeng/keyfilter';

import {InputSwitchModule} from 'primeng/inputswitch';
import {MatTooltipModule} from '@angular/material/tooltip';

import { ParentDashboardComponent } from './parent-dashboard/parent-dashboard.component';
import { AddKidComponent } from './add-kid/add-kid.component';
import { AddRulesComponent } from './add-rules/add-rules.component';
import { KidCardComponent } from './kid-card/kid-card.component';
import { AddRewardsComponent } from './add-rewards/add-rewards.component';
import { SettingsComponent } from './settings/settings.component';
import { SelectRewardTypeComponent } from './settings/select-reward-type/select-reward-type.component';
import { ExtaChoresComponent } from './exta-chores/exta-chores.component';
import { DailyChoresComponent } from './daily-chores/daily-chores.component';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ParentRoutingModule,
    // Material
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatTableModule,
    MatCardModule,
    MatCheckboxModule,
    KeyFilterModule,
    // PrimenNg
    DropdownModule,
    TableModule,
    FieldsetModule,
    SliderModule,
    DialogModule,
    InputTextModule,
    CalendarModule,
    CardModule,
    PanelModule,
    ButtonModule,
    AccordionModule,
    MatTooltipModule,
    // Others
    InputSwitchModule,
    FontAwesomeModule,
    SharedModule
  ],
  declarations: [
    ParentDashboardComponent, 
    AddKidComponent, 
    AddRulesComponent, 
    KidCardComponent, 
    AddRewardsComponent, 
    SettingsComponent, 
    SelectRewardTypeComponent, 
    ExtaChoresComponent, 
    DailyChoresComponent, 
    ResetPasswordComponent
  ],
  entryComponents: [
    AddKidComponent,
    SettingsComponent,
    ExtaChoresComponent,
    DailyChoresComponent,
    ResetPasswordComponent
  ],
  providers: [
    DynamicDialogConfig,
    DynamicDialogRef
  ]
})
export class ParentDashboardModule { }
