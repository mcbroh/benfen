import { Component, OnInit, Input, AfterContentInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { faPlusSquare, faMinusSquare } from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';

import { GET_CURRENT_USER } from '../../../Apollo';
import { DataService } from '../../shared/service/data.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Apollo } from 'apollo-angular';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-daily-chores',
  templateUrl: './daily-chores.component.html',
  styleUrls: ['./daily-chores.component.scss']
})
export class DailyChoresComponent implements OnInit, AfterContentInit {

  @Input() chore: object;
  @Input() label: object;
  @Input() data: object;
  @Input() userName: string;

  componentMainForm: FormGroup;
  icon = {
    add: faPlusSquare,
    remove: faMinusSquare
  }
  processing: boolean; 
  choreId = null
  setChore = new BehaviorSubject(true)
  kidsConnectedToChore = [];

  constructor(
    private apollo: Apollo, 
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private config: DynamicDialogConfig,
    private dialogRef: DynamicDialogRef
  ) { 
    this.config['data'] && this.setDialogState();
  }

  ngOnInit() {
    this.createForm()
  }

  ngAfterContentInit(): void {
    this.chore && this.data['kids'].forEach(kid => {
      kid['dailyChores'].includes(this.chore['id']) && this.kidsConnectedToChore.push(kid['username'])
    })    
  }

  setDialogState() {
    this.config.header = this.config['data']['header'];
    this.label = this.config['data']['label']; 
    this.data = this.config['data']['appData']; 
  }

  private createForm() { 
    this.componentMainForm = this.formBuilder.group({
      choreName: ['', Validators.required],
      choreOpen: ['07:00', Validators.required],
      choreClose: ['11:00', Validators.required],
      chores: this.formBuilder.array([ this.createConditions('Lay your bed') ]),
    })
    this.chore && this.addItem();
    this.subscribeToFormChanges();
  }

  subscribeToFormChanges() {
    this.componentMainForm.get('choreOpen').valueChanges.subscribe(time => {
      if (this.isBadTimeFormat(time)) this.componentMainForm.get('choreOpen').setValue(this.correctTimeFormat(time)); 
    })
    this.componentMainForm.get('choreClose').valueChanges.subscribe(time => {
      if (this.isBadTimeFormat(time)) this.componentMainForm.get('choreClose').setValue(this.correctTimeFormat(time)); 
    })
  }

  isBadTimeFormat(time: string): boolean {
    const splitedDate = time.split(':').join('').split('');
    let retVal = false
    if (splitedDate.length > 0) {
      if (splitedDate[0] !== '_' && +splitedDate[0] > 2) retVal = true;
      if (splitedDate[1] !== '_' && +splitedDate[0] > 1 && +splitedDate[1] > 3) retVal = true;
      if (splitedDate[2] !== '_' && +splitedDate[2] > 5) retVal = true;
    }
    return retVal
  }

  correctTimeFormat(time: string): string {
    const splitedDate = time.split('');
      if (splitedDate[0] !== '_' && +splitedDate[0] > 2) splitedDate[0] = '2';
      if (splitedDate[1] !== '_' && +splitedDate[0] > 1 && +splitedDate[1] > 3) splitedDate[1] = '3';
      if (splitedDate[3] !== '_' && +splitedDate[3] > 5) splitedDate[3] = '5';
      for (let index = 0; index < splitedDate.length; index++) {
         if (splitedDate[index] === '_') splitedDate[index] = '0';
      }
    return splitedDate.join('')
  }

  private addItem(): void {
    this.componentMainForm['controls']['choreName'].setValue(this.chore['name']);
    this.componentMainForm['controls']['choreOpen'].setValue(this.chore['open']);
    this.componentMainForm['controls']['choreClose'].setValue(this.chore['close']);
    const items = this.componentMainForm.get('chores') as FormArray;
    items.removeAt(0);
    this.chore['chores'].forEach(chore => {
      items.push(this.createConditions(chore))
    });
  }
  
  public addEmptyItem() {
    const items = this.componentMainForm.get('chores') as FormArray;
    items.push(this.createConditions(''))
  }
  private createConditions = (val): FormGroup => this.formBuilder.group({ rule: [val, Validators.required] });



  addNewChore() {    
    this.processing = true;
    const dataToMutate = this.componentMainForm.value; 

    const choreArr = dataToMutate.chores.map(chore => chore['rule']);
    dataToMutate['chores'] = choreArr;
    dataToMutate['id'] = moment().format("YYYYMMDDHHMMSS");
    
    this.dataService.httpUpdate('family/daily-chore', dataToMutate).subscribe(res => {
      if (res.success) {        
        if (this.data['dailyChoreData'].length > 0)  this.dataService.updateApolloCache(res['data']);
        else this.getUser();
        this.choreId = dataToMutate['id'];
        if (this.data['kids'].length > 0) this.setChore.next(false);
        else this.dialogRef.close()
      }
      this.processing = false;
    });  
  }

  connectedKidToChore(username, checked) {
    if (checked) this.kidsConnectedToChore.push(username);
    else this.kidsConnectedToChore = this.kidsConnectedToChore.filter(user => user !== username);
  }

  addKidToChore() {
    this.processing = true;
    const dataToMutate = {
      choreId: this.choreId || this.chore['id'],
      users: this.kidsConnectedToChore
    }
    this.dataService.httpUpdate('family/add-chore-to-kid', dataToMutate).subscribe(res => {
      if (res.success) {  
        this.processing = false;      
        this.dataService.updateApolloCache(res['data']);
        !this.chore && this.dialogRef.close()
      }
    });  
  }

  updateChore() {
    this.processing = true;
    const dataToMutate = this.componentMainForm.value; 

    const choreArr = dataToMutate.chores.map(chore => chore['rule']);
    dataToMutate['chores'] = choreArr;
    dataToMutate['id'] = this.chore['id'];
    
    this.dataService.httpUpdate('family/daily-chore', dataToMutate).subscribe(res => {
      if (res.success) {    
        this.processing = false;    
        this.dataService.updateApolloCache(res['data']);
      }
    }); 
  }

  getUser() {    
    this.apollo.query({
      query: GET_CURRENT_USER,
      fetchPolicy: 'network-only'
    }).subscribe();
  }

}
