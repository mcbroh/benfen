import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentDashboardComponent } from './parent-dashboard/parent-dashboard.component'


const routes: Routes = [
    {
        path: '',
        component: ParentDashboardComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ParentRoutingModule { }
