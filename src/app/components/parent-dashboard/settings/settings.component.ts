import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/service/data.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { BehaviorSubject } from 'rxjs';
import { AppDialogService } from 'src/app/service/dialog.service';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';

const basicObject = {
  used: false,
  useGameTime: false,
  useMoney: false,
  point: 0,
  rewardMinute: 0,
  rewardAmount: 0
};

@Component({
  selector: 'app-parent-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  public label;
  processing = new BehaviorSubject<boolean>(false);
  
  public study: object = {...basicObject} 
  public dailyChores: object = {...basicObject} 
  public bonusChores: object = {...basicObject}

  constructor(
    private dataService: DataService,
    private config: DynamicDialogConfig,
    private dialogRef: DynamicDialogRef,
    private appDialogService: AppDialogService
  ) {}

  ngOnInit() {
    this.label = this.config['data']['label']; 
    this.study = this.config['data']['state']['study'];
    this.dailyChores = this.config['data']['state']['dailyChores'];
    this.bonusChores = this.config['data']['state']['bonusChores'];   
  }

  openResetPass() {
    this.dialogRef.close();
    this.appDialogService.openComponent(ResetPasswordComponent);
  }

  actionUpdate() {
  
    this.processing.next(true);
    const dataToMutate = {
      study: {...this.study, point: this.getRandomInt()},
      dailyChores: {...this.dailyChores, point: this.getRandomInt()},
      bonusChores: {...this.bonusChores, point: this.getRandomInt()},
    }
    this.dataService.httpUpdate('family', dataToMutate).subscribe(res => {
      if (res.success) {
        this.dataService.updateApolloCache(res['data'])
        this.dialogRef.close()
      }
      this.processing.next(false);
      
    });
    
    
  }

  getRandomInt = () => Math.floor(Math.random() * Math.floor(10));
  
  

}

