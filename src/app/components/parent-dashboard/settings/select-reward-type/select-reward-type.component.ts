import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-select-reward-type',
  templateUrl: './select-reward-type.component.html',
  styleUrls: ['../settings.component.scss']
})
export class SelectRewardTypeComponent {
  
  @Input() label: object;
  @Input() parentObj: BehaviorSubject<object>;

  @Output() useGameTime = new EventEmitter<boolean>();
  @Output() useMoney = new EventEmitter<boolean>();
  @Output() rewardAmount = new EventEmitter<number>();
  @Output() rewardminutes = new EventEmitter<number>();

  onChangeGameTime(value: boolean) { this.useGameTime.emit(value); }
  onChangeMoneyReward(value: boolean) { this.useMoney.emit(value); }
  onChangeMoneyAmount(value: number) { this.rewardAmount.emit(value); }
  onChangeMinute(value: number) { this.rewardminutes.emit(value); }
}
