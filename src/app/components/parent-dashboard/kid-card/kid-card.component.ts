import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Apollo } from 'apollo-angular';
import { BehaviorSubject, Subscription } from 'rxjs';

import { DataService } from '../../shared/service/data.service';
import { GET_MY_SUBJECTS, GET_CURRENT_USER } from '../../../Apollo';


interface SubjectObject {
  name: string; 
  hardnessMin: number; 
  hardnessMax: number; 
  amountOfQuestion: number; 
  timeToGain: number
}

interface MyChoresObj { id: string; name: string; open: string; close: string; chores: [string]}


@Component({
  selector: 'app-kid-card',
  templateUrl: './kid-card.component.html',
  styleUrls: ['./kid-card.component.scss']
})
export class KidCardComponent implements OnInit, OnDestroy {
  @Input() userName;
  data;
  kid;
  @Input() label;
  displayedColumns: string[] = ['open', 'closed', 'todo'];

  updatingUser = new BehaviorSubject(false);
  subjects = new BehaviorSubject([{ label: 'Loading.....', value: 'null' }]);
  
  testInfo = {
    minutes: 60,
    amount: 10,
    min: 2,
    max: 10
  }

  querySub: Subscription;

  val: number = 0;
  selectedSubject: string;
  subjectsCols: { field: string; header: string; }[] = [];
  mySubjects: Array<SubjectObject> = null;
  choresCols: { field: string; header: string; }[] = [];

  myChores: [MyChoresObj];
  loading: boolean = true;

  constructor(
    private apollo: Apollo,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private dataService: DataService,
  ) {
    
   }

  ngOnInit() { 
    this.getUserData();
  }

  getUserData() {
    this.querySub = this.apollo.watchQuery({
      query: GET_CURRENT_USER,
      fetchPolicy: 'cache-only'
    }).valueChanges.subscribe(({ data, loading }) => {
      if (!loading) {    
        this.kid = data['getCurrentUser']['kids'].filter(kid => kid['username'].toLowerCase() === this.userName.toLowerCase())[0];        
        this.data = data['getCurrentUser']
        this.myChores = <any>[]
        this.data['dailyChores']['used'] && this.setKidsChores();
        this.data['study']['used'] && this.getSubjects();
        this.data['study']['used'] && this.getMySubjects(); 
        this.setTableCols();
        this.loading = loading
      }
    });
  }

  setKidsChores() {
    if (this.kid.dailyChores && this.kid.dailyChores.length > 0 ) {
      this.data.dailyChoreData.forEach(chore => { 
        this.kid.dailyChores.includes(chore['id']) && this.myChores.push({
          id: chore['id'], 
          name: chore['choreName'], 
          open: chore['choreOpen'], 
          close: chore['choreClose'], 
          chores: chore['chores']
        })
      })
    }
  }

  setTableCols() {
    this.choresCols = [
      { field: 'name', header: 'Name' },
      { field: 'open', header: 'Open' },
      {field: 'close', header: 'Closed' },
    ];
    this.subjectsCols = [
      { field: 'name', header: 'Subject' },
      { field: 'hardnessMin', header: 'HardnessMin' },
      { field: 'hardnessMax', header: 'HardnessMax' },
      { field: 'amountOfQuestion', header: 'TotalQuestions' },
      { field: 'timeToGain', header: 'TimeGained' }
    ];
  }

  getSubjects() {
    this.dataService.httpGet('subjects').subscribe((res) => {      
     this.setSubjectsToDropdown(res['data'])
    });
  }

  setSubjectsToDropdown(subjects: Array<any>) {
    const dropdownArr = subjects.map(subject => ({
      label: subject['name'].split(' ')[0],
      value: subject['name'].split(' ')[0].toLowerCase(),
    }))
    this.selectedSubject = dropdownArr[0]['value'];
    this.subjects.next(dropdownArr);    
  }



  updateChildStudy() {
    const dataToMutation: SubjectObject = {
      name: this.selectedSubject,
      hardnessMin: this.testInfo['min'],
      hardnessMax: this.testInfo['max'],
      amountOfQuestion: this.testInfo['amount'],
      timeToGain: this.testInfo['minutes']
    }
    this.updatingUser.next(true)
    this.dataService.httpUpdate(
      'subjects/my-subjects', 
      {username: this.kid['username'], subject: dataToMutation}
    ).subscribe((res) => {
      this.updatingUser.next(false)
     this.snackBar.open(res['message'], null, { panelClass: ['snack-' + res['status']] });
     res['success'] && this.getMySubjects('network-only')
    });
  }

  getMySubjects = async (fetchPolicy: any = 'cache-first') => {
    const username = this.kid['username']
    this.apollo.query({
      query: GET_MY_SUBJECTS,
      fetchPolicy: fetchPolicy
    }).subscribe(({ data }) => {      
      data && (
        data['getCurrentUser']['kids'].forEach(kid => {
          if (kid['username'].toLowerCase() === username.toLowerCase()) {
            this.mySubjects = this.kid['subjects'] || null
          }
        })
      );
    });
  };

  choreTrackByFn = (index, item) => item.id;
  subjectTrackByFn = (index, item) => item.id;

  ngOnDestroy() {
    this.querySub && this.querySub.unsubscribe()
  }


}
