import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Apollo } from 'apollo-angular-boost';
import { Subscription, BehaviorSubject } from 'rxjs';

import { GET_CURRENT_USER } from '../../../Apollo';
import { LanguageService } from 'src/app/service/language.service';
import { AppDialogService } from 'src/app/service/dialog.service';
import { SettingsComponent } from '../settings/settings.component';
import { AddKidComponent } from '../add-kid/add-kid.component';
import { ExtaChoresComponent } from '../exta-chores/exta-chores.component';
import { DailyChoresComponent } from '../daily-chores/daily-chores.component';
import { HttpErrorResponse, HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-parent-dashboard',
  templateUrl: './parent-dashboard.component.html', 
  styleUrls: ['../parent-dashboard.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParentDashboardComponent implements OnInit, OnDestroy {
  
  addKid: boolean = false;
  loading: boolean = true;
  editRules: boolean = false;
  data;

  langSub: Subscription;
  querySub: Subscription;
  lang = new BehaviorSubject<any>({});
  usedModules = { 
    study: false, 
    bonusChores: false,  
    dailyChores: false, 
  };

  constructor(
    private apollo: Apollo, 
    private httpClient: HttpClient,
    private cdr: ChangeDetectorRef,
    private langService: LanguageService,
    private appDialogService: AppDialogService
  ) { 
    this.langSub = this.langService.selectedLang.subscribe(lang => { this.lang.next(lang['parentModule']) })
    this.getUserData();
    this.httpClient.get('https://find.biltema.com/v1/web/reko/100/1/sv/FAK541').subscribe(x => {
      this.httpClient.get('https://reko.biltema.com/v1/Reko/extendedcarinfo/FAK541/1/sv').subscribe(k => {
      console.log(x['serviceIntervalCategories'], k['nameOfTheCar']);
      
    })
      
    })
    //https://reko.biltema.com/v1/Reko/extendedcarinfo/USK540/1/sv
  }

  ngOnInit() { }

  getUserData() {
    this.querySub = this.apollo.watchQuery({
      query: GET_CURRENT_USER,
      fetchPolicy: 'cache-and-network'
    }).valueChanges.subscribe(({ data, loading }) => {
      if (data) {    
        console.log({data});
            
        this.usedModules = {
          study: data['getCurrentUser']['study']['used'] || false,
          bonusChores: data['getCurrentUser']['bonusChores']['used'] || false,
          dailyChores: data['getCurrentUser']['dailyChores']['used'] || false
        };
        this.data = data['getCurrentUser']
      }
      
      this.loading = loading;
      this.cdr.markForCheck()
    });
  }

  openSettingsDialog() {
    this.appDialogService.data = {
      label: this.lang.value,
      state: this.data
    }
    this.appDialogService.openComponent(SettingsComponent, this.lang.value.settings);
  }

  showAddChildDialog() {
    this.appDialogService.data = {
      label: this.lang.value,
      kids: this.data['kids']
    }
    this.appDialogService.openComponent(AddKidComponent, this.lang.value.addChild)
  }

  showAddExChoresDialog() {
    this.appDialogService.data = {
      label: this.lang.value,
      header: this.lang.value.addExChores,
    }
    this.appDialogService.openComponent(ExtaChoresComponent)
  }

  showAddDayChoresDialog() {
    this.appDialogService.data = {
      label: this.lang.value,
      appData: this.data,
      header: this.lang.value.addDayChores,
    }
    this.appDialogService.openComponent(DailyChoresComponent)
  }

  ngOnDestroy() {
    this.querySub && this.querySub.unsubscribe();
    this.langSub.unsubscribe();
  }

}
