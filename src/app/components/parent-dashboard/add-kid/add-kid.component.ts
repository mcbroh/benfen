import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import isAlphanumeric from 'validator/lib/isAlphanumeric';

import { MutationService } from '../../../service/mutation.service'
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-add-kid',
  templateUrl: './add-kid.component.html',
  styleUrls: ['./add-kid.component.scss']
})
export class AddKidComponent implements OnInit {
  
  label: object;
  kids: Array<object>;

  addKidForm: FormGroup;
  selected = 'morning';
  displayedColumns: string[] = ['period', 'open', 'closed'];
  choresTime = {
    morning: {open: '06:00', closed: '11:00' },
    evening: {open: '16:00', closed: '20:30' },
  };

  dayTime = [ ];

  userNameError: boolean;
  processing: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private mutator: MutationService,
    private dialogRef: DynamicDialogRef,
    private config: DynamicDialogConfig
  ) { }

  ngOnInit() {
    this.label = this.config['data']['label']; 
    this.kids = this.config['data']['kids']; 
    this.dayTime = [
        {label: this.label['morning'], value: 'morning'},
        {label: this.label['evening'], value: 'evening'},
    ]
    const ChoreTiming = localStorage.getItem('ChoreTiming');    
    ChoreTiming && (this.choresTime = JSON.parse(ChoreTiming));
    this.createForm()
  }

  updateTime(time, dayTime) {
    this.choresTime[this.selected][dayTime] = time
    localStorage.setItem('ChoreTiming', JSON.stringify(this.choresTime))
  }

  setTime() {
    const values = this.addKidForm.value;
    this.selected !== 'none' && (this.choresTime[this.selected] = {
      open: values.open,
      closed: values.closed
    })
    this.selected === 'none' && (
      this.addKidForm.controls['open'].setValue(this.choresTime[this.selected]['open']),
      this.addKidForm.controls['closed'].setValue(this.choresTime[this.selected]['closed'])
    )
  }

  dropdownChanged(value) {    
    this.addKidForm.controls['open'].setValue(this.choresTime[value]['open']),
    this.addKidForm.controls['closed'].setValue(this.choresTime[value]['closed'])
  }

  private createForm() {
    this.addKidForm = this.formBuilder.group({
      pass: ['', Validators.required],
      username: new FormControl('', {validators:  Validators.compose([
        Validators.required,
        this.validateFamilyName
      ]), updateOn: 'blur'})
    })
  }

  private createChores = (val, dayVal = 'morning'): FormGroup => this.formBuilder.group({ 
    chore: [val, Validators.required], 
    dayTime:  [dayVal, Validators.required]
  });

  public addEmptyInput() {
    const items = this.addKidForm.get('houseChores') as FormArray;
    items.push(this.createChores(''));
  }

  validateFamilyName(controls) {
    if (isAlphanumeric(controls.value)) return null;
    return {'validateEmail': true}
  } 

  // compare value and return true or false
  compareValue = (mainString: string, valueToTest: string)  => mainString.toLocaleLowerCase() === valueToTest.toLocaleLowerCase();

  addHouseRules() {
    const mutationDetails = this.addKidForm.value;  
    // Check if name or username already exist
    this.kids.forEach(kid => {
    this.userNameError = this.compareValue(kid['username'], mutationDetails['username']);
    })
    if (this.userNameError) { return };

    this.runMutation();
  }

  runMutation() {
     const mutationDetails = this.addKidForm.value; 
     if (
       mutationDetails['pass'] === '' ||
       mutationDetails['username'] === ''
       ) {
         return;
       }
      
       this.processing = true;
    this.mutator.actionMutate(
      'ADD_FAMILY_MEMBER', 
      {
        pass: mutationDetails['pass'],
        name: mutationDetails['username'].toLocaleLowerCase(), 
        username: mutationDetails['username'].toLocaleLowerCase(),
      },
      'House rules added',
      [{query: 'GET_CURRENT_USER'}]
    ).subscribe(x => {
      this.dialogRef.close()
    });
  }

}
