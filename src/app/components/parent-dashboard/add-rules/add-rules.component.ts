import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { faPlusSquare, faMinusSquare } from '@fortawesome/free-solid-svg-icons';

import { MutationService } from '../../../service/mutation.service'

@Component({
  selector: 'app-add-rules',
  templateUrl: './add-rules.component.html',
  styleUrls: ['./add-rules.component.scss']
})
export class AddRulesComponent implements OnInit {
  @Output() rulesSet = new EventEmitter();
  @Input() rules;
  @Input() label;
  rulesForm: FormGroup;
  icon = {
    add: faPlusSquare,
    remove: faMinusSquare
  }
  processing: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private mutator: MutationService,
  ) { }

  ngOnInit() {
    this.createForm()
  }

  addHouseRules() {
    this.processing = true;
    const mutationDetails = this.rulesForm.value;    
    this.mutator.actionMutate(
      'ADD_HOUSE_RULES', 
      {houserules: mutationDetails['houseRules'].map(x => x['rule'])},
      'House rules added',
      [{query: 'GET_CURRENT_USER'}]
    ).subscribe(x => {
      this.processing = false
      this.rulesSet.emit(false)
    })
  }

  private createForm() { 
    this.rulesForm = this.formBuilder.group({
      houseRules: this.formBuilder.array([ this.createConditions('Our first house rule') ]),
    })
    this.rules.length > 0 && this.addItem();
  }

  private addItem(): void {
    const items = this.rulesForm.get('houseRules') as FormArray;
    items.removeAt(0);
    this.rules.forEach(x => {
      items.push(this.createConditions(x))
    });
  }
  
  public addEmptyItem() {
    const items = this.rulesForm.get('houseRules') as FormArray;
    items.push(this.createConditions(''))
  }
  private createConditions = (val): FormGroup => this.formBuilder.group({ rule: [val, Validators.required] });

}
