import { Component, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { Apollo } from 'apollo-angular';
import { DynamicDialogConfig, ConfirmationService } from 'primeng/api';
import { BehaviorSubject, Subscription } from 'rxjs';

import { GET_CURRENT_USER } from '../../../Apollo';
import { DataService } from '../../shared/service/data.service';

@Component({
  selector: 'app-exta-chores',
  templateUrl: './exta-chores.component.html',
  styleUrls: ['./exta-chores.component.scss']
})
export class ExtaChoresComponent implements OnDestroy {

  public label;
  public data;
  processing = new BehaviorSubject<boolean>(false);
  todo: string = '';
  rewardAmount: string = '00'
  rewardMinute: string = '00'
  querySub: Subscription;
  bonusChores: any;
  
  constructor(
    private apollo: Apollo,
    private dataService: DataService,
    private config: DynamicDialogConfig,
    private confirmationService: ConfirmationService
  ) { 
    this.config.header = this.config['data']['header'];
    this.label = this.config['data']['label']; 
    this.getChores();
  }

  getChores() {
    this.querySub = this.apollo.watchQuery({
      query: GET_CURRENT_USER,
      fetchPolicy: 'cache-first'
    }).valueChanges.subscribe(({ data }) => {            
      if (data) {
        this.data = data['getCurrentUser']['extraChore'];
        this.bonusChores = data['getCurrentUser']['bonusChores'];
      }
    });
  };

  actionCreate() {
    this.processing.next(true);
    const dataToMutate = {
      id: moment().format("YYYYMMDDHHMMSS"),
      todo: this.todo,
      rewardAmount: +this.rewardAmount < 1 ? this.bonusChores['rewardAmount'] : +this.rewardAmount,
      rewardMinute: +this.rewardMinute < 1 ? this.bonusChores['rewardMinute'] : +this.rewardMinute, 
      done: false,
      repeat: true
    }
    this.dataService.httpUpdate('family/extra-chore', dataToMutate).subscribe(res => {
      if (res.success) {
        this.todo = '';
        this.rewardAmount = '00';
        this.rewardMinute = '00';
        if (this.data.length > 0) this.dataService.updateApolloCache(res['data']);
        else this.refetch();
        
      }
      this.processing.next(false);
    });
  }

  updateChoreState(choreId, state) {
    this.dataService.httpUpdate('family/update-extra-chore', {id: choreId, state: !state}).subscribe(res => {
      if (res.success) {
        this.dataService.updateApolloCache(res['data'])
      }
      this.processing.next(false);
    });
  }

  deleteChore(id) {
    this.confirmationService.confirm({
      message: 'Delete chore!',
      accept: () => {
        this.dataService.httpUpdate('family/delete-extra-chore', {id}).subscribe(res => {
          if (res.success) {            
            this.refetch()
          }
        });
      }
  });
  }

  refetch() {    
    this.apollo.query({
      query: GET_CURRENT_USER,
      fetchPolicy: 'network-only'
    }).subscribe();
  }

  ngOnDestroy(): void {
    this.querySub.unsubscribe();
  }

}
