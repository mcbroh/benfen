import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FamilyTimeRoutingModule } from './family-time-routing.module';

import { FamilyTimeDashboardComponent } from './family-time-dashboard/family-time-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    FamilyTimeRoutingModule
  ],
  declarations: [FamilyTimeDashboardComponent]
})
export class FamilyTimeModule { }
