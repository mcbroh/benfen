import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyTimeDashboardComponent } from './family-time-dashboard.component';

describe('FamilyTimeDashboardComponent', () => {
  let component: FamilyTimeDashboardComponent;
  let fixture: ComponentFixture<FamilyTimeDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyTimeDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyTimeDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
