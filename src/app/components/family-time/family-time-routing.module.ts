import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FamilyTimeDashboardComponent } from './family-time-dashboard/family-time-dashboard.component';


const routes: Routes = [
    {
        path: '',
        component:  FamilyTimeDashboardComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class FamilyTimeRoutingModule { }
