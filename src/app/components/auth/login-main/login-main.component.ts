import { Component, OnDestroy, ChangeDetectionStrategy } from '@angular/core';

import { LanguageService } from 'src/app/service/language.service';
import { Subscription, BehaviorSubject } from 'rxjs';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-login-main',
  templateUrl: './login-main.component.html',
  styleUrls: ['./login-main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginMainComponent implements OnDestroy {

  langSub: Subscription;
  viewstate = {
    childPanelOpenState: true,
    parentPanelOpenState: false
  }

  formTypes = new BehaviorSubject<SelectItem[]>([]) ;
  selectedType: string = 'child';
  formDisplayed = new BehaviorSubject<object>(this.viewstate)
  
  lang = new BehaviorSubject<any>({});
  
  constructor( 
    private langService: LanguageService
  ) { 
    this.langSub = this.langService.selectedLang.subscribe(lang => { 
      this.lang.next(lang['auth']);
      this.setFormTypes(lang['auth']);
    })
  }

  private setFormTypes(lang: object): void {
    const types = [
      {label: lang['child'], value: 'child'},
      {label: lang['parent'], value: 'parent'}
    ]
    this.formTypes.next(types) 
  }


  public formTypeChange(value): void {    
    value === 'child' ? this.fieldSetstate('childPanelOpenState') : this.fieldSetstate('parentPanelOpenState');
  }

  private fieldSetstate(formToShow): void {
    Object.keys(this.viewstate).forEach(key => { this.viewstate[key] = false; })
    this.viewstate[formToShow] = true;
    this.formDisplayed.next(this.viewstate)    
  }

  ngOnDestroy() {
    this.langSub.unsubscribe();
  }

}
