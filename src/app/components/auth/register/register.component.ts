import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Apollo } from 'apollo-angular-boost';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { BehaviorSubject, Subscription } from 'rxjs';

import {
  validateAlphaNumeric, validateForm, changeFormState, validateEmail, mustMatch
} from '../../../_helpers/index';
import { SIGNUP_USER } from '../../../Apollo';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent implements OnInit {

  // Form related
  form: FormGroup
  messageClass;
  message;
  langSub: Subscription;
  processing = new BehaviorSubject(false);
  lang = new BehaviorSubject<any>({});

  constructor(
    private apollo: Apollo,
    private router: Router,
    public snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private langService: LanguageService
  ) {
    this.langSub = this.langService.selectedLang.subscribe(lang => { 
      this.lang.next(lang['auth']);
      this.initForm();
    })
  }

  ngOnInit() { }

  initForm() {
    this.form = this.formBuilder.group({
      familyName: ['', Validators.compose([Validators.required, Validators.minLength(4), validateAlphaNumeric])],
      email: ['', Validators.compose([Validators.required, validateEmail])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    }, {
        validator: mustMatch('password', 'confirmPassword'), 
        updateOn: 'blur'
      })

    this.form.controls['familyName']['label'] = this.lang.value["username"]
    this.form.controls['familyName']['hint'] = "axelsson1";
    this.form.controls['password']['label'] = this.lang.value["password"];
    this.form.controls['email']['label'] = "Email"
    this.form.controls['confirmPassword']['label'] = this.lang.value["passwordConfirm"];
  }

  onRegisterSubmit() {
    validateForm(this.form);
    if (this.form.valid) {
      this.processing.next(true);
      const user = {
        familyName: this.form.get('familyName').value,
        email: this.form.get('email').value,
        password: this.form.get('password').value,
      };
      changeFormState(this.form, 'disable');
      this.apollo.mutate({
        mutation: SIGNUP_USER,
        variables: user
      }).subscribe(({ data }) => {
        this.processing.next(false);        
        this.snackBar.open(data['signupUser']['token'], null, { panelClass: ['snack-success'] });
        this.router.navigate(['/auth/login'])
      }, (error) => {
        changeFormState(this.form, 'enable');
        this.processing.next(false);          
        this.messageClass = 'alert alert-warning';
        this.message = error['message'].slice(error['message'].indexOf(':') + 2)
      });
    }
  }

}
