import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatDialogModule, MatSnackBarModule } from '@angular/material';
import {FieldsetModule} from 'primeng/fieldset';
import {SelectButtonModule} from 'primeng/selectbutton';
import {ToastModule} from 'primeng/toast';

import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';


import { ParentLoginComponent } from './parent-login/parent-login.component';
import { RegisterComponent } from './register/register.component';
import { LoginMainComponent } from './login-main/login-main.component';
import { ChildLoginComponent } from './child-login/child-login.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    // Material
    MatDialogModule,
    MatSnackBarModule,

    // prime
    SelectButtonModule,
    FieldsetModule,
    ToastModule,

    // Local
    SharedModule
  ],
  declarations: [
    ParentLoginComponent, 
    RegisterComponent, 
    LoginMainComponent, 
    ChildLoginComponent
  ],
  entryComponents: [
    ParentLoginComponent, 
    ChildLoginComponent
  ]
})
export class AuthModule { }
