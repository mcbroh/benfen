import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular-boost';

import { LOGIN_KID, GET_CURRENT_USER } from '../../../Apollo';
import { AuthGuard } from '../../../guard/auth.guard.service';
import { validateForm, changeFormState } from '../../../_helpers/index';
import { BehaviorSubject, Subscription } from 'rxjs';
import { LanguageService } from 'src/app/service/language.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-child-login',
  templateUrl: './child-login.component.html',
})
export class ChildLoginComponent implements OnInit {

  // Form related
  form: FormGroup;
  processing = new BehaviorSubject(false);

  // Route related
  previousUrl;
  langSub: Subscription;
  lang = new BehaviorSubject<any>({});

  constructor(
    private apollo: Apollo,
    private router: Router,
    private authGuard: AuthGuard,
    private formBuilder: FormBuilder,
    private langService: LanguageService,
    private messageService: MessageService
  ) {
    this.langSub = this.langService.selectedLang.subscribe(lang => { this.lang.next(lang['auth']); })
  }

  ngOnInit() {
    this.initForm();
    if (this.authGuard.redirectedUrl) {
      this.previousUrl = this.authGuard.redirectedUrl;
      this.authGuard.redirectedUrl = undefined;
    }
  }

  initForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    }, { updateOn: 'blur' });
    this.form.controls['username']['hint'] = "noel@axelsson";
  }

  loginUser() {
    validateForm(this.form);

    if (this.form.valid) {

      this.processing.next(true);
      changeFormState(this.form, 'disable');

      // start process
      const user = {
        id: this.form.get('username').value,
        pass: this.form.get('password').value,
      };

      this.apollo.mutate({
        mutation: LOGIN_KID, 
        variables: user
      }).subscribe(({ data}) => {
        data['loginKid']['success'] ? this.setAppForUser(data['loginKid']) : this.showMessage(data['loginKid']['message'])  
      }, (error) => {
        this.showMessage(error['message'].slice(error['message'].indexOf(':') + 2));
      });

    }
  }

  setAppForUser(data: object): void {
    localStorage.setItem('token', data['token']);
    localStorage.setItem('type', data['type']);
    localStorage.setItem('username', data['username']);
    this.getCurrentUserDtails();
  }

  getCurrentUserDtails() {
    this.apollo.query({
      query: GET_CURRENT_USER
    }).subscribe(({ data }) => {      
      if (data['getCurrentUser']) {
        if (this.previousUrl) this.router.navigate([this.previousUrl]);
        else this.router.navigate(['/sweethome']);
      }
    });
  };

  showMessage(msg) {    
    this.processing.next(false);
    changeFormState(this.form, 'enable');
    this.messageService.add({severity: 'error', summary: 'Error', detail: msg});
  }
}

