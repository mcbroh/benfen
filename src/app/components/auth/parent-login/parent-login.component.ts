import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular-boost';
import { BehaviorSubject, Subscription } from 'rxjs';

import { SIGNIN_USER } from '../../../Apollo';
import { AuthGuard } from '../../../guard/auth.guard.service';
import { validateForm, changeFormState, validateEmail } from '../../../_helpers/index';
import { LanguageService } from 'src/app/service/language.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-parent-login',
  templateUrl: './parent-login.component.html'
})
export class ParentLoginComponent implements OnInit {

  // Form related
  form: FormGroup;
  processing = new BehaviorSubject<boolean>(false);
  // Route related
  previousUrl;
  langSub: Subscription;
  lang = new BehaviorSubject<object>({});

  constructor(
    private apollo: Apollo,
    private router: Router,
    private authGuard: AuthGuard,
    private formBuilder: FormBuilder,
    private langService: LanguageService,
    private messageService: MessageService
  ) {
    this.langSub = this.langService.selectedLang.subscribe(lang => { 
      this.lang.next(lang['auth']);
    })
  }

  ngOnInit() {
    this.initForm();
    if (this.authGuard.redirectedUrl) {
      this.previousUrl = this.authGuard.redirectedUrl;
      this.authGuard.redirectedUrl = undefined;
    }
  }

  initForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, validateEmail])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    }, { updateOn: 'blur' });
    this.form.controls['email']['label'] = "Email"
  }

  loginUser() {
    validateForm(this.form)

    if (this.form.valid) {

      this.processing.next(true);
      changeFormState(this.form, 'disable');

      // start process
      const user = {
        email: this.form.get('email').value,
        password: this.form.get('password').value,
      };

      this.apollo.mutate({
        mutation: SIGNIN_USER,
        variables: user
      }).subscribe(({ data }) => {
        data['signinUser']['success'] ? this.setAppForUser(data['signinUser']) : this.showMessage(data['signinUser']['message']);
      }, (error) => {
        this.showMessage(error['message'].slice(error['message'].indexOf(':') + 2));
      });

    }
  }

  setAppForUser(data: object): void {
    setTimeout(() => {
      if (this.previousUrl) this.router.navigate([this.previousUrl]);
      else {
        data['type'] === 'parent' && this.router.navigate(['/dashboard']);
      }
    }, 1000);
    localStorage.setItem('token', data['token'])
    localStorage.setItem('type', data['type'])
  }

  showMessage(msg) {    
    this.processing.next(false);
    changeFormState(this.form, 'enable');
    this.messageService.add({severity: 'error', summary: 'Error', detail: msg});
  }
}
