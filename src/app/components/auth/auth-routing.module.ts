import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginMainComponent, RegisterComponent } from '../auth';


const routes: Routes = [
    {
        path: '',
        component: LoginMainComponent
    },
    {
        path: 'login',
        component: LoginMainComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }
