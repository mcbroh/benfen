import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

interface DialogData {
  userName: string;
}

@Component({
  selector: 'app-add-study-dialog',
  templateUrl: './add-study-dialog.component.html',
  styleUrls: ['./add-study-dialog.component.scss']
})
export class AddStudyDialogComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

}
