import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'; 

import {CardModule} from 'primeng/card';
import {DialogModule} from 'primeng/dialog';
import {ProgressBarModule} from 'primeng/progressbar';

import { SharedModule } from '../shared/shared.module' 
import { StudyRoutingModule } from "./study.routing.module";
import { StudyComponent } from './study.component';


@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,
    StudyRoutingModule,
    ProgressBarModule,
    SharedModule,
    CardModule,
    DialogModule,
  ],
  declarations: [
    StudyComponent,
  ]
})
export class StudyModule { }
