import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DataService } from '../shared/service/data.service';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { ConfirmationService } from 'primeng/api';

import { GET_CURRENT_USER } from '../../Apollo';
import { Apollo } from 'apollo-angular';
import { MatSnackBar } from '@angular/material';


interface Test {
  x: number;
  y: number;
  hint: number;
  points: number;
  sign: string;
  id: string
}

@Component({
  selector: 'app-study',
  templateUrl: './study.component.html',
  styleUrls: ['./study.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudyComponent implements OnInit, OnDestroy {

  loadingAsync = new BehaviorSubject(false)
  loadingTime = new BehaviorSubject(false)
  disabled = new BehaviorSubject(null);
  passwordMsg = new BehaviorSubject(null);
  animating = new BehaviorSubject(true);
  subjects = new BehaviorSubject([]);
  displayTestmodal = false

  // enable point
  gameTimeEnable = false;
  testTimeLeft = new BehaviorSubject({full: 0, min: 0, sec: 0});
  waitTimeLeft = new BehaviorSubject({full: 0, min: 0, sec: 0});
  testInterval;
  waitInterval;
  animationInterval;

  useTimeDialog = false;
  stop = 0;
  loops = 0
  flashed = 0;
  testQuestions = new BehaviorSubject([]);
  testQuestionsCopy: any;
  linkName: string;
  parentPassword: string = '';
  gameTime =  0;
  timeToUse = 0;
  testTime = 0;

  testsTimeToGain = {}
  sendOnce: boolean;

  constructor(
    private apollo: Apollo,
    private confirmationService: ConfirmationService,
    private dataService: DataService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
      this.getMyGameTime();
      this.getSubjects();
  }

  getMyGameTime = async (fetchPolicy: any = 'cache-first') => {
    const username = await localStorage.getItem('username');
    this.apollo.query({
      query: GET_CURRENT_USER,
      fetchPolicy: fetchPolicy
    }).subscribe(({ data }) => {  
         
      data && (
        data['getCurrentUser']['kids'].forEach(kid => {
          if (kid['username'].toLowerCase() === username.toLowerCase()) {
            
            this.gameTime = +kid['gameTime'];

            (kid['subjects'] || []).forEach(subject => {
              this.testsTimeToGain[subject['name']] = +subject['timeToGain']
            });            

            (kid['testResults'] || []).forEach(result => {
              localStorage.setItem(
                result['subject'], 
                JSON.stringify({ percentageOfCorrectAnswer: result['score'] })
              );
            });
          }
        })
      );
    });
  };

  checkPassword() {
    this.dataService.httpUpdate('math/password-check', {password: this.parentPassword}).subscribe((res) => {
      res['success'] && this.startTimedTest();
      !res['success'] && this.snackBar.open('Wrong Password', null, { panelClass: ['snack-error'] });
    });
  }

  startTimedTest() {
    this.parentPassword = '';
    this.displayTestmodal = false;
    this.getTimedTest();
    this.startTimer(this.convertMinToHr(this.testTime));
  }

  convertMinToHr = (min: number): number => min * 60;

  getTimedTest() {
    const randomIndex = Math.floor(Math.random() * this.subjects.value.length);
    this.setLinkAndGetTest(this.subjects.value[randomIndex]['link']);
    this.disabled.next(this.subjects.value[randomIndex]['link']);
  }

  startTimer(minutes: number): void {
    this.gameTimeEnable = true;
    this.saveTimeToStorage(minutes)
    this.testTimeLeft.next({full: minutes, min: Math.floor(minutes / 60), sec: this.getNumAsDouble(minutes % 60) });
    this.testInterval = setInterval(() => {
      if (this.testTimeLeft.value.full > 0) {
        const fullTime = this.testTimeLeft.value.full - 1;        
        this.testTimeLeft.next({full: fullTime, min: Math.floor(fullTime / 60), sec: this.getNumAsDouble(fullTime % 60) });
      } else {
        this.sendOnce = true;
        this.correctTest()
        this.clearTestTimmer();
      }
    }, 1000)
  }

  clearTestTimmer() {
    this.gameTimeEnable = false;
    this.testTimeLeft.next({full: 0, min: 0, sec: 0});
    clearInterval(this.testInterval);
  }

  getSubjects() {
    this.dataService.httpGet('subjects').subscribe((res: Test[]) => {
      res['data'].length > 0 && this.setTestQuestions(res['data']);
    });
  }
  setTestQuestions(subjects) {    
    this.subjects.next(subjects);
    this.loadingAsync.next(false);
    this.loops = Math.floor(Math.random() * 50);

    if (subjects.length === 1) this.setLinkAndGetTest(subjects[0]['link']);
    else this.runAnimation();
    
    
  }

  runAnimation() {
    this.animationInterval = setInterval(() => {
      this.flashText()
    }, 200);
  }

  flashText() {
    this.stop += 1;
    const subjects = this.subjects.value,
      lastFlashed = this.flashed;
    this.disabled.next(subjects[lastFlashed]['link']);
    lastFlashed < subjects.length - 1 ? this.flashed += 1 : this.flashed = 0;
    this.stop === this.loops && this.setLinkAndGetTest(subjects[lastFlashed]['link']);
  }

  setLinkAndGetTest(link) {
    clearInterval(this.animationInterval);
    this.linkName = link;
    setTimeout(() => {
      this.getTest();
    }, 500);

  }

  getTest(): void {
    this.dataService.httpGet(this.linkName).subscribe((res: Test[]) => {
      const questionsToDisplay = res['data'].map(question => ({
        ...question,
        answer: '',
        wrong: false,
        right: false
      }))

      this.testQuestions.next(questionsToDisplay)
      this.animating.next(false);
    });
  }

  correctTest() {
    let noOfCorretAnswers = 0,
      noOfWrongAnswers = 0;

    this.testQuestions.value.forEach(question => {

      const userAnswerIsCorrect = +question['answer'] === eval(question['x'] + question['sign'] + question['y']);

      if (question['answer'] !== '' && userAnswerIsCorrect) {
        question['right'] = true;
        noOfCorretAnswers += 1;
      } else {
        question['wrong'] = true;
        noOfWrongAnswers += 1;
      }
    });
    
    // Get time gained from answeres
    const testName = this.linkName.split('=')[1];
    // tslint:disable-next-line:max-line-length
    const av = new CalculateAvergeTimeGained(noOfCorretAnswers, noOfWrongAnswers, this.linkName, +this.testsTimeToGain[testName], this.sendOnce);

    const result = av.getAverageTimeGained();
    if (result['updateServer']) this.confirmationSaveTest(result);
    else this.confirmRetakeTest(result['result']);

  }

  confirmationSaveTest(result) {
    this.confirmationService.confirm({
      message: 'You gained: ' +  result['timeGained'] + ' min(s) game time! \n Save time?',
      header: 'Result: ' + result['result'],
      icon: 'pi pi-exclamation-triangle',
      accept: () => { this.setNewTestRecord(result['percentageOfCorrectAnswer'], result['timeGained']) },
      reject: () => { this.getTest() }
    });
  }

  confirmRetakeTest(result) {
    this.confirmationService.confirm({
      message: 'Retake test: You need to beat old score',
      header: 'Result: ' + result,
      icon: 'pi pi-exclamation-triangle',
      accept: () => { this.getTest() },
      reject: () => { /* this.getTest() */ },
    });
  }

  setNewTestRecord(percentageOfCorrectAnswer, timeGained) {
    // this.getTest();
    if (this.sendOnce || this.gameTimeEnable) {
      console.log(this.sendOnce, this.gameTimeEnable);
      
      this.resetTimerAndLockTest()
      this.sendOnce = false;
      this.putResultToserver({
        timeGained,
        test: {
          subject: this.linkName,
          score: percentageOfCorrectAnswer
        }
      })
    };
  }
  
  resetTimerAndLockTest() {
    this.clearTestTimmer();
    this.startWaitTimer();
  }

  startWaitTimer() {
    const full = 320;
    localStorage.setItem('testTimeEnd', this.saveTimeToStorage(full));
    this.waitTimeLeft.next({full, min: Math.floor(full / 60), sec: this.getNumAsDouble(full % 60) });
    this.waitInterval = setInterval(() => {
      if (this.compareRealTime(localStorage.getItem('testTimeEnd')) > 0) {
        const fullTime = this.compareRealTime(localStorage.getItem('testTimeEnd'));
        this.waitTimeLeft.next({full: fullTime, min: Math.floor(fullTime / 60), sec: this.getNumAsDouble(fullTime % 60) });
      } else {
        this.clearWaitTimmer();
      }
    }, 1000)
  }

  clearWaitTimmer() {
    this.testTimeLeft.next({full: 0, min: 0, sec: 0});
    clearInterval(this.testInterval);
  }

  saveTimeToStorage = (seconds: number): string => moment().add(seconds, 'seconds').format('HH:mm:ss')
  

  compareRealTime = (time: string): number => {
    const now = moment();
    const end = moment(time, 'HH:mm:ss');
    return end.diff(now, 'seconds')
  }
  getNumAsDouble = (num: number): any => num.toString().length === 1 ? '0' + num : num;

  putResultToserver(variables) {
    this.dataService.httpUpdate('math', variables).subscribe((res) => {
      this.getMyGameTime('network-only');
    });
  }

  closeDialog() {
    this.useTimeDialog = false;
    this.passwordMsg.next(null)
  }

  grantGameTime() {
    this.loadingTime.next(true);
    this.passwordMsg.next(null)
    this.dataService.httpUpdate('math/grant-time', {password: this.parentPassword, timeToUse: this.timeToUse}).subscribe((res) => {
      if (res['success']) {
        this.closeDialog();
        this.getMyGameTime('network-only');
        this.snackBar.open('Start Timer for ' + this.timeToUse + ' mins', null, { panelClass: ['snack-success'] });
      } else {
        this.loadingTime.next(false)
        this.passwordMsg.next(res['message']);
      };
      
    });
  }

  ngOnDestroy() { }
}

class CalculateAvergeTimeGained {

  testTaken: string
  totalNumQuestions: number;
  totalNumOf_Correct_Answers: number;
  totalNumOf_Wrong_Answers: number;
  visibleResult: string;
  useTimeDialog: boolean

  constructor(
    protected correctAnswer: number,
    protected noOfWrongAnswers: number,
    protected testName: string,
    protected timeToGain: number,
    protected timed: boolean
  ) {
    this.totalNumQuestions = correctAnswer + noOfWrongAnswers;
    this.totalNumOf_Correct_Answers = correctAnswer;
    this.totalNumOf_Wrong_Answers = noOfWrongAnswers;
    this.testTaken = testName;
    this.timeToGain = timeToGain;
    this.useTimeDialog = timed;
    this.visibleResult = `${correctAnswer} / ${correctAnswer + noOfWrongAnswers}`;
    
  }

  getAverageTimeGained() {
    // const fullMinuteAsReward = 15,
      const percentageOfCorrectAnswer = (this.totalNumOf_Correct_Answers / this.totalNumQuestions) * 100,
      timeGained = Math.floor(this.timeToGain * (percentageOfCorrectAnswer / 100));

    if (percentageOfCorrectAnswer === 100) {
      return { result: this.visibleResult, timeGained, updateServer: true };
    }

    return this.checkLastTestTaken(percentageOfCorrectAnswer, timeGained)

  }


  checkLastTestTaken(percentageOfCorrectAnswer, timeGained) {
    const prevSubjectTestResult = JSON.parse(localStorage.getItem(this.testTaken))

    if (prevSubjectTestResult) {

      if (this.useTimeDialog || (percentageOfCorrectAnswer > prevSubjectTestResult['percentageOfCorrectAnswer'])) {
        return { percentageOfCorrectAnswer, result: this.visibleResult, timeGained, updateServer: true };
      } else {
        return { percentageOfCorrectAnswer, result: this.visibleResult, timeGained: 0, updateServer: false };
      }
    }
    return { percentageOfCorrectAnswer, result: this.visibleResult, timeGained, updateServer: true };
  }

}
