import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {

  persons: Array<object> = [
    {
      country: 'Sweden', 
      contact: 'name.surname@benfen.com',
      greeting: 'This is a wider card with supporting text below as a natural lead-in to additional content.',
      name: 'Jam Miu',
      position: 'Fullstack Developer',
      image: '/assets/noImage.png'
    },
    {
      country: 'Sweden', 
      contact: 'name.surname@benfen.com',
      greeting: 'This is a wider card with supporting text below as a natural lead-in to additional content.',
      name: 'Jam Miu',
      position: 'Fullstack Developer',
      image: '/assets/noImage.png'
    },
    {
      country: 'Sweden', 
      contact: 'name.surname@benfen.com',
      greeting: 'This is a wider card with supporting text below as a natural lead-in to additional content.',
      name: 'Jam Miu',
      position: 'Fullstack Developer',
      image: '/assets/noImage.png'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
