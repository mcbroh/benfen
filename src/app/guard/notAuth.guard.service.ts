import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthService } from '../service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class NotAuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  canActivate() {    
    if (this.authService.loggedIn()) {  
      this.authService.isParent() && this.router.navigate(['dashboard']);  
      !this.authService.isParent() && this.router.navigate(['sweethome']);  
      return false;
    } else {
      return true;
    }
  }
}
