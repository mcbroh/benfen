import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from '../service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  redirectedUrl; 
  
  constructor(
    public authservice: AuthService,
    private router: Router,
  ) { }

  canActivate(
    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {    
    if (this.authservice.loggedIn()) {      
       return true;
    } else {
      this.redirectedUrl = state.url;
      this.router.navigate(['/auth/login']);
      return false;
    }
  }
}
