import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from '../service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ChildNavigation implements CanActivate {

  redirectedUrl; 
  
  constructor(
    public authservice: AuthService,
    private router: Router,
  ) { }

  canActivate(
    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {    
    if (!this.authservice.isParent()) {      
       return true;
    } else {
      this.router.navigate(['/dashboard']);
      return false;
    }
  }

}
