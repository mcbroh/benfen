import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './components/home/home.component';
import { SupportComponent } from './components/support/support.component';

import { AuthGuard } from './guard/auth.guard.service';
import { NotAuthGuard } from './guard/notAuth.guard.service';
import { ChildNavigation } from './guard/child.nav.guard';

const appRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [NotAuthGuard] 
    },
    {
        path: 'dashboard',
        loadChildren: '../app/components/parent-dashboard/parent-dashboard.module#ParentDashboardModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'sweethome',
        loadChildren: '../app/components/child-dashboard/child-dashboard.module#ChildDashboardModule',
        canActivate: [AuthGuard, ChildNavigation]
    },
    {
      path: 'study',
      loadChildren: '../app/components/study/study.module#StudyModule', 
      canActivate: [AuthGuard, ChildNavigation]
    },
    {
        path: 'familytime',
        loadChildren: '../app/components/family-time/family-time.module#FamilyTimeModule',
        canActivate: [AuthGuard]
    },
    {
      path: 'auth',
      loadChildren: '../app/components/auth/auth.module#AuthModule',
      canActivate: [NotAuthGuard]
    },
    {
        path: 'support',
        component: SupportComponent,
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        component: HomeComponent
    }
];



@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules, scrollPositionRestoration: 'enabled'})],
    providers: [],
    bootstrap: [],
    exports: [RouterModule]
})

export class AppRoutingModule { }
