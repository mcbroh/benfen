import * as moment from 'moment';

const now = moment().format("HH:mm")
const today = moment().format("YYYY-MM-DD")

export const nowIsBefore = (arg: string): boolean => +arg.split(':').join('') > +now.split(':').join('');

export const nowIsAfter = (arg: string): boolean => +arg.split(':').join('') < +now.split(':').join('');

export const isSameDay = (arg: string): boolean => +arg.split('-').join('') === +now.split('-').join('');
