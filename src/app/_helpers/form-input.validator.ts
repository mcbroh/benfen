import { AbstractControl, FormGroup } from '@angular/forms';
import isAlphanumeric from 'validator/lib/isAlphanumeric';
import isEmail from 'validator/lib/isEmail';

export const validateEmail = (control: AbstractControl) => isEmail(control.value) ? null : { 'email': true };

export const validateAlphaNumeric = (control: AbstractControl) => isAlphanumeric(control.value)  ?  null : {'alphanumeric': true};


// custom validator to check that two fields match
export const mustMatch = (controlName: string, matchingControlName: string) =>
  (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    // return if another validator has already found an error on the matchingControl
    if (matchingControl.errors && !matchingControl.errors.mustMatch) return;

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
