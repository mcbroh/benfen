import { FormGroup, FormControl } from "@angular/forms";

export const changeFormState = (form: FormGroup, state: string) => {
  Object.keys(form.controls).forEach(field => {
    const control = form.get(field);
    if (control instanceof FormControl) {
      state === 'disable' ? control.disable() : control.enable();
    } else if (control instanceof FormGroup) {
      this.changeFormState(control);
    }
  });
}
