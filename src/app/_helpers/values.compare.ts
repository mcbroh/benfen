/* UserName compare */
export const isCorrectUsername = (arg1: string, arg2: string): boolean => arg1.toLowerCase() === arg2.toLowerCase();
