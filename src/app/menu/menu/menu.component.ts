import { Component, Input, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ScrollPanel } from 'primeng/scrollpanel';

import { AppComponent } from '../../app.component';
import { AuthService } from 'src/app/service/auth.service';
import { Subscription } from 'rxjs';
import { LanguageService } from 'src/app/service/language.service';
import { GET_CURRENT_USER } from 'src/app/Apollo';
import { Apollo } from 'apollo-angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements AfterViewInit, OnDestroy {

  @Input() reset: boolean;

  menuItems: any[];
  lang: object;
  langSub: Subscription;
  authSub: Subscription;
  useStudy: boolean;

  @ViewChild('layoutMenuScroller') layoutMenuScrollerViewChild: ScrollPanel;

  constructor(
    private apollo: Apollo,
    public app: AppComponent,
    private authService: AuthService,
    private langService: LanguageService
  ) { 
    this.langSub = this.langService.selectedLang.subscribe(lang => {             
      this.lang = lang['menu'];
      this.setMenuItems() 
    })
    this.authSub = this.authService.listenToAuthStatus().subscribe(() => { this.setMenuItems() })
  }

  setMenuItems() {
    this.menuItems = [
      {
        label: 'LAYOUT', icon: 'fa fa-fw fa-desktop',
        items: [
          { label: 'Static', icon: 'fa fa-fw fa-bars', command: event => this.app.changeMenuMode('static') },
          { label: 'Overlay', icon: 'fa fa-fw fa-bars', command: event => this.app.changeMenuMode('overlay') },
          { label: 'Slim', icon: 'fa fa-fw fa-bars', command: event => this.app.changeMenuMode('slim') },
          { label: 'Horizontal', icon: 'fa fa-fw fa-bars', command: event => this.app.changeMenuMode('horizontal') }
        ]
      }
    ];
    this.setDynamicRoutes();
  }

  private setDynamicRoutes(): void {    
    if (this.authService.loggedIn()) {
      this.getMyGameTime();
      this.menuItems.unshift({ label: this.lang['familyTime'], icon: 'fa fa-fw fa-users', routerLink: ['/familytime'] })
      if (this.authService.isParent()) {
        this.menuItems.unshift({ label: this.lang['dashBoard'], icon: 'fa fa-fw fa-home', routerLink: ['/dashboard'] })
      } else {
        this.useStudy && this.menuItems.unshift({label: this.lang['study'], routerLink: ['/study'], icon: 'fa fa-fw fa-gamepad' });
        this.menuItems.unshift({label: this.lang['dashBoard'], icon: 'fa fa-fw fa-home', routerLink: ['/sweethome']});
      }
      this.menuItems.push({ label: this.lang['logout'], icon: 'fa fa-fw fa-sign-out-alt', command: (event) => this.authService.logout() })
    } else {
      this.menuItems.push(
        { label: this.lang['login'], icon: 'fa fa-fw fa-sign-in-alt', routerLink: ['/auth/login'] },
        { label: this.lang['register'], icon: 'fa fa-fw fa-user-plus', routerLink: ['/auth/register'] },
      );
    }
  }

  getMyGameTime = async () => {
    const username = await localStorage.getItem('username');
    username && this.apollo.query({
      query: GET_CURRENT_USER,
      fetchPolicy: 'cache-first'
    }).subscribe(({ data }) => {      
      if (data) {        
        data['getCurrentUser']['kids'].forEach(kid => {
          if (kid['username'] === username) {
            this.useStudy = data['getCurrentUser']['study']['used'] && kid['subjects'].length > 0 || false;
          }
        })
      };
    });
  };

  ngAfterViewInit() {
    setTimeout(() => { this.layoutMenuScrollerViewChild.moveBar(); }, 100);
  }

  /* changeTheme(theme: string) {
    const layoutLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('layout-css');
    layoutLink.href = 'assets/layout/css/layout-' + theme.split('-')[0] + '.css';
    const themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
    themeLink.href = 'assets/theme/' + 'theme-' + theme + '.css';
  } */

 /*  changeTopbarColor(topbarColor, logo) {
    this.app.topbarColor = topbarColor;
    const topbarLogoLink: HTMLImageElement = <HTMLImageElement>document.getElementById('topbar-logo');
    topbarLogoLink.src = 'assets/layout/images/' + logo + '.png';
  } */

  onMenuClick(event) {
    if (!this.app.isHorizontal()) {
      setTimeout(() => {
        this.layoutMenuScrollerViewChild.moveBar();
      }, 450);
    }
    this.app.onMenuClick(event);
  }

  ngOnDestroy() {
    this.langSub.unsubscribe();
    this.authSub.unsubscribe();
  }
}



