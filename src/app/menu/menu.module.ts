import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SlideMenuModule } from 'primeng/slidemenu';
import { ContextMenuModule } from 'primeng/contextmenu';
import { MegaMenuModule } from 'primeng/megamenu';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { TabMenuModule } from 'primeng/tabmenu';
import { TieredMenuModule } from 'primeng/tieredmenu';

import { FooterComponent } from './footer/footer.component';
import { TopbarComponent } from './topbar/topbar.component';
import { MenuComponent } from './menu/menu.component';
import { AppSubMenuComponent } from './menu/submenu.component';

@NgModule({
  imports: [
    CommonModule,
    ScrollPanelModule,
    SlideMenuModule,
    ContextMenuModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    PanelMenuModule,
    TabMenuModule,
    TieredMenuModule
  ],
  declarations: [
    FooterComponent, 
    TopbarComponent, 
    MenuComponent,
    AppSubMenuComponent
  ],
  exports: [
    FooterComponent, 
    TopbarComponent, 
    MenuComponent,
    AppSubMenuComponent
  ]
})
export class AppMenuModule { }
