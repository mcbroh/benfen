import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { AuthService } from 'src/app/service/auth.service';
import { Subscription } from 'rxjs';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnDestroy, OnInit {
  
  authSub: Subscription;
  isLoggedIn: boolean;
  showReward = null;
  showGameTime = null;

  constructor(
    public app: AppComponent,
    public authService: AuthService,
    public lang: LanguageService
  ) { }

  ngOnInit(): void {
    this.showReward = this.authService.reward;
    this.showGameTime = this.authService.gameTime;
    this.authSub = this.authService.listenToAuthStatus().subscribe(status => {
      this.isLoggedIn = status;
      status && this.authService.getCurrentUserDetails();
    })
    
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  } 

}
