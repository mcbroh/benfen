// import gql from 'graphql-tag';
import { gql } from "apollo-angular-boost";
const BasicObjectType = `{
  used
  useGameTime
  useMoney
  point
  rewardMinute
  rewardAmount
}`;

export const GET_CURRENT_USER = gql`
	query {
		getCurrentUser {
      id
			familyName
			email
			avatar
			type
      houserules
      rewardDay
			kids{
				name
				pass
				username
				savedPoints
        savedForTheMonth
        dateOfBirth
        gameTime
        dailyChores
        choresDone
        subjects {
          name
          timeToGain
          hardnessMin
          hardnessMax
          amountOfQuestion
        }
				chores{
					rewardOpenFrom
					rewardClosed
					toDo
					type
				}
				points{
					end
					start
					title
					month
					color{
						primary
						secondary
					}
				}
			}
			extraChore {
				id
				todo
        done
        rewardAmount
        rewardMinute
			}
			familyTime {
				id
				time
				todo
				day
			}
			rewards{
				name
				color
				start
				end
				title
      }
      dailyChoreData {
        id
        choreName
        choreOpen
        choreClose
        chores
      }
      study ${BasicObjectType}
      dailyChores ${BasicObjectType}
      bonusChores ${BasicObjectType}
		}
	}
`;

export const GET_MY_SUBJECTS = gql`
  query {
    getCurrentUser {
      id
      kids {
        username
        savedForTheMonth
        subjects {
          name
          timeToGain
          hardnessMin
          hardnessMax
          amountOfQuestion
        }
      }
    }
  }
`;

export const GET_MY_CHORES = gql`
  query {
    getCurrentUser {
      id
      kids {
        username
        chores {
          rewardOpenFrom
          rewardClosed
          toDo
          type
        }
      }
    }
  }
`;

/* User Mutations */
export const SIGNIN_USER = gql`
  mutation($email: String!, $password: String!) {
    signinUser(email: $email, password: $password) {
      token
      type
      message
      success
    }
  }
`;

export const LOGIN_KID = gql`
  mutation($id: String!, $pass: String!) {
    loginKid(id: $id, pass: $pass) {
      token
      type
      username
      message
      success
    }
  }
`;

export const SIGNUP_USER = gql`
  mutation($familyName: String!, $email: String!, $password: String!) {
    signupUser(familyName: $familyName, email: $email, password: $password) {
      token
    }
  }
`;
export const UPDATE_USER_AVARTAR = gql`
  mutation UpdateUserAvatar($file: String!) {
    userAvatar(file: $file) {
      avatar
    }
  }
`;

export const ADD_HOUSE_RULES = gql`
  mutation($houserules: [String!]) {
    addHouseRule(houserules: $houserules) {
      houserules
    }
  }
`;

export const ADD_FAMILY_MEMBER = gql`
  mutation(
    $name: String!
    $pass: String!
    $username: String!
    $chores: [ChoreInput]
  ) {
    addFamilyMember(
      name: $name
      pass: $pass
      username: $username
      chores: $chores
    ) {
      kids {
        name
        pass
        savedPoints
        savedForTheMonth
        username
        chores {
          rewardOpenFrom
          rewardClosed
          toDo
          type
        }
      }
    }
  }
`;

export const ADD_POINT = gql`
  mutation(
    $end: String!
    $start: String!
    $title: String!
    $month: String!
    $password: String!
    $gameTime: Int!
    $color: [colorInput]
  ) {
    updateChildPoint(
      end: $end
      start: $start
      title: $title
      password: $password
      color: $color
      gameTime: $gameTime
      month: $month
    ) {
      kids {
        name
        pass
        savedPoints
        savedForTheMonth
        username
        gameTime
        chores {
          rewardOpenFrom
          rewardClosed
          toDo
        }
        points {
          end
          start
          title
          month
          color {
            primary
            secondary
          }
        }
      }
    }
  }
`;

export const CASH_OUT_KID_POINTS = gql`
  mutation($pointsToUse: Int!, $password: String!) {
    useChildPoint(pointsToUse: $pointsToUse, password: $password) {
      kids {
        name
        pass
        savedPoints
        savedForTheMonth
        username
      }
    }
  }
`;

export const SAVE_KID_POINT_WITH_BONUS = gql`
  mutation($bonusPoint: Int!, $password: String!) {
    savePointTillNextMonthChildPoint(
      bonusPoint: $bonusPoint
      password: $password
    ) {
      kids {
        name
        pass
        savedPoints
        savedForTheMonth
        username
      }
    }
  }
`;
