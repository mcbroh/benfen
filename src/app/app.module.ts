import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// Apollo
import { ApolloBoostModule, ApolloBoost } from "apollo-angular-boost";
import { MaterialModule } from "./material.module";
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from "@angular/material";

import { ConfirmDialogModule } from "primeng/confirmdialog";
import { MenuModule } from "primeng/menu";
import { MenubarModule } from "primeng/menubar";

import { CalendarModule, DateAdapter } from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";
import { DynamicDialogModule } from "primeng/dynamicdialog";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { HomeComponent } from "./components/home/home.component";

import { SupportComponent } from "./components/support/support.component";
import { SettingUpImageComponent } from "./components/dialogs/setting-up-image/setting-up-image.component";
import { CalenderComponent } from "./components/calender/calender.component";

import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { SharedModule } from "./components/shared/shared.module";
import { AppMenuModule } from "./menu/menu.module";
import { ConfirmationService, MessageService } from "primeng/api";
import { AddStudyDialogComponent } from "./components/dialogs/add-study-dialog/add-study-dialog.component";
import { ParentDashboardModule } from "./components/parent-dashboard/parent-dashboard.module";
import { ToastModule } from "primeng/toast";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SupportComponent,
    SettingUpImageComponent,
    CalenderComponent,
    AddStudyDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    // HttpModule,
    HttpClientModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    ApolloBoostModule,
    SharedModule,
    AppMenuModule,

    // primeng
    DynamicDialogModule,
    ConfirmDialogModule,
    ToastModule,
    MenuModule,
    MenubarModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    Ng4LoadingSpinnerModule.forRoot(),
    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production
    }),
    ParentDashboardModule
  ],
  entryComponents: [SettingUpImageComponent, AddStudyDialogComponent],
  providers: [
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 2500,
        verticalPosition: "top",
        horizontalPosition: "right"
      }
    },
    ConfirmationService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(apollo: ApolloBoost) {
    const request = async operation => {
      let context;
      const token = await localStorage.getItem("token");
      context = {
        headers: {
          auth: token
        }
      };
      if (!token) context = {};
      operation.setContext(context);
    };
    apollo.create({
      uri: "https://benfen.herokuapp.com/graphql", // uncomment for production
      // uri: "http://localhost:4444/graphql", // uncomment for development
      request: request,
      onError: ({ graphQLErrors, networkError }) => {
        if (graphQLErrors) {
          console.log({ graphQLErrors });

          console.log({ graphQLErrors: "Should be relogged in" });
          /* sendToLoggingService(graphQLErrors); */
        }
        if (networkError) {
          console.log({ networkError });

          console.log({ networkError: "user should be logged out" });

          // logoutUser();
        }
      }
    });
  }
}
