import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { menu } from './langPack/menu';
import { auth } from './langPack/auth';
import { emptyState } from './langPack/emptyState';
import { parentModule } from './langPack/parentModule';
import { childModule } from './langPack/childModule';

@Injectable({ providedIn: 'root' })
export class LanguageService {

  private displayedLang = localStorage.getItem('@language') || 'swedish';
  private langObj: object;
  private lang = new BehaviorSubject<object>({});
  selectedLang = this.lang.asObservable();

  constructor() {
    this.setLanguage(this.displayedLang);
  }

  setLanguage(langugage: string) {
    this.langObj = {
        menu: menu[langugage],
        auth: auth[langugage],
        emptyState: emptyState[langugage],
        parentModule: parentModule[langugage],
        childModule: childModule[langugage]
    }
    this.lang.next(this.langObj);
  }

  changeLang(language: string): void { 
    this.setLanguage(language);
    localStorage.setItem('@language', language)
  }
  
}
