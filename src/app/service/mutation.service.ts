import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import {Apollo} from 'apollo-angular-boost';
import { MatSnackBar } from '@angular/material'

import * as mutationType from '../Apollo';

@Injectable({
  providedIn: 'root'
})
export class MutationService {

  constructor(
    private apollo: Apollo,
    public snackBar: MatSnackBar
  ) {  }

  actionMutate(mutationName, variables, successMsg, refetchQuery = []) {
    const refetchQueries = refetchQuery.map(x => {
     return { ...x, ...{ query: mutationType[x['query']]} }
    });
    
    return this.apollo.mutate({
      mutation: mutationType[mutationName],
      variables,
      refetchQueries: refetchQueries /*  [{ query: GET_MESSAGE }] */
    }).pipe(map(({data, loading, error}) => {
      this.snackBar.open(successMsg, null, { panelClass: ['snack-success'] });
      return {data, loading, error}
    }, (error) => {
      this.snackBar.open(error['message'].slice(error['message'].indexOf(':') + 2), null, { panelClass: ['snack-fail'] });
    }));
  }

}
