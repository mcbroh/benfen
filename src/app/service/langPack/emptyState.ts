export const emptyState = {
  english : {
    start: 'Start with',
    left: 'left',
    right: 'right',
    topLeft: 'top left',
    topRight: 'top right',
    kidCard: 'Start by adding a chore or study'
  },
  swedish : {
    start: 'Börja med',
    left: 'vänster',
    right: 'höger',
    topLeft: 'övre vänster',
    topRight: 'högra höger',
    kidCard: 'Börja med att lägga in sysslor eller studier'
  }
}
