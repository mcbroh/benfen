export const menu = {
  english : {
    login: 'Login', register: 'Register account', logout: 'Logout', gameTime: 'GAME TIME',
    dashBoard: 'DASHBOARD', familyTime: 'Family Time', study: 'Study'
  },
  swedish : {
    login: 'Logga in', register: 'Skapa konto', logout: 'Logga ut', gameTime: 'SPELTID',
    dashBoard: 'HEM', familyTime: 'Familjetid', study: 'Studera'
  }
}
