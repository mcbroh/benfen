import { Injectable } from '@angular/core';
import { DialogService } from 'primeng/api';


@Injectable({  providedIn: 'root' })

export class AppDialogService extends DialogService {

  public data = {};
  private params = {
    contentStyle: {
      "width": "790px",
      "max-height": "80vh", 
      "max-width": "90vw", 
      "overflow": "auto"
    }
  }

  openComponent(Component, header =  'Choose Header') {
    return this.open(Component, {...this.params, data: this.data, header});
    
  }


}
