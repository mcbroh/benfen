import { Injectable, OnDestroy } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Router, NavigationStart } from '@angular/router';
import { Subject, Observable, Subscription, BehaviorSubject } from 'rxjs';

import { GET_CURRENT_USER } from '../Apollo';
const basicObject = {
  show: false,
  value: null
};
@Injectable({ providedIn: 'root' })
export class AuthService implements OnDestroy {
  authToken;
  options;
  private authStatus = new Subject<boolean>();
  public reward = new BehaviorSubject<object>({...basicObject});
  public gameTime = new BehaviorSubject<object>({...basicObject});

  querySub: Subscription;
  subscription: Subscription;

  constructor(
    private apollo: Apollo,
    private router: Router,
  ) { 
    this.setAuthState();
    this.subscription = router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.setAuthState();
      }
  });
  }

  setAuthState() {
    localStorage.getItem('token') ? this.authStatus.next(true) : this.authStatus.next(false)
  }

  public loadToken = () => this.authToken = localStorage.getItem('token')

  public logout() {
    this.authToken = null;
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('uuid');
    localStorage.removeItem('type');
    this.reward.next(basicObject);
    this.gameTime.next(basicObject);
    this.router.navigate(['/auth/login']).then(() => {
      this.apollo.getClient().resetStore().then(() => window.location.reload());
    });
  }
  public loggedIn = () => localStorage.getItem('token') ? true : false; // tokenNotExpired();
  public isParent = () => localStorage.getItem('type') === 'parent' ? true : false;

  listenToAuthStatus(): Observable<any> {
    return this.authStatus.asObservable();
  }

  getCurrentUserDetails() {    
    this.isParent() === false && this.allowChildToSubscribe();
  };

  allowChildToSubscribe() {  
    const username = localStorage.getItem('username');
    this.querySub = this.apollo.watchQuery({
      query: GET_CURRENT_USER,
      fetchPolicy: 'cache-only'
    }).valueChanges.subscribe(({ data }) => {     
      if (data['getCurrentUser'] && this.isParent() === false) {
        // Compare name
        data['getCurrentUser']['kids'].forEach(kid => {
          if (kid['username'].toLowerCase() === username.toLowerCase()) {
            this.reward.next({
              show: data['getCurrentUser']['dailyChores']['used'],
              value: + (kid['savedPoints'] || 0)
            });
            this.gameTime.next({
              show: this.bothMustBeTruety(data['getCurrentUser']) ,
              value: + (kid['gameTime'] || 0)
            })
          }
        });
      }  
    });
  }

  returnAtleastOneTrue(data: object): boolean {
    if (data['dailyChores']['used']
        || data['extraChore']['used']
        || data['study']['used']
      ) {
      return true;
    }
    return false;
  }

  returnAtleastOneTrueForChild(data: object): boolean {
    if (data['dailyChores']['useGameTime']
        || data['extraChore']['useGameTime']
        || data['study']['useGameTime']
      ) {
      return true;
    }
    return false;
  }

  bothMustBeTruety(data: object): boolean {    
    if (this.returnAtleastOneTrue(data) && this.returnAtleastOneTrueForChild(data)) {
      return true;
    }
    return false;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.querySub && this.querySub.unsubscribe();
  }

}
