const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const jwt = require('jsonwebtoken');

require('dotenv').config({path: 'variables.env'});
const User = require('./api/models/User');

//  GraphQL-Expressmiddleware
const { graphiqlExpress, graphqlExpress} = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');

const { typeDefs } = require('./schema');
const {resolvers } = require('./resolvers');

const schema = makeExecutableSchema({
	typeDefs,
	resolvers
});

// Connect to database
mongoose
	.connect(process.env.MONGO_URI)
	.then(() => console.log('DB connected'))
	.catch(err => console.error(err));

// Initialize app
const app = express();

app.use(cors('*'));

// Set up JWT authentication middleware
app.use(async (req, res, next) => {
	const token = req.headers['auth'];
	if (token) {		
		try {
			const currentUser = await jwt.verify(token, process.env.SECRET);
			req.currentUser = currentUser;
		} catch (err) {
			console.error({Error: 'No Token Provided'})
		}
	}
	next();
});

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false }));
app.use(express.static(__dirname + '/dist/'));

require('./api/models/Subjects');
app.use(require('./api/routes'));

// let application use graphql UNCOMENT FOR Production
//app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql'}));

// Connect schemas with graphql

app.use(
	'/graphql', 
	bodyParser.json(),
	graphqlExpress(({ currentUser }) => ({
		schema,
		context: {
			User,
			currentUser
		}
	}))
);


  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/dist/'))
  });

const PORT = process.env.PORT

app.listen(PORT, () => {
  console.log(`Server listning on PORT ${PORT}`);
});