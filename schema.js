exports.typeDefs = `
	type ColorObjType {
		primary: String
		secondary: String
	}
	type CalenderObjectType {
		color: [ColorObjType]
		month: String
		end: String
		start: String
		title: String
	}
	type ChoreType {
		rewardOpenFrom: String
		rewardClosed: String
		toDo: String
		type: String
  }
  type testResultType {
		subject: String
		score: String
  }
  type subjectType {
    name: String
    hardnessMin: String
    hardnessMax: String
    amountOfQuestion: String
    timeToGain: String
  }

	type KidType {
		name: String! 
		pass: String!
		savedForTheMonth: Int
		dateOfBirth: String
    username: String!
    subjects: [subjectType]
    testResults: [testResultType]
		points: [CalenderObjectType]
    chores: [ChoreType]
    dailyChores: [String]
    gameTime: Int,
    savedPoints: Int
    choresDone: [String]
	}
	type RewardType {
		name: String! 
		start: String!
		end: String!
		color: String! 
		title: String!
	}

	type ChoreTodoType {
		id: ID
		name: String
		end: String
		start: String
		points: String
		todos: [String]
		connectedKids: [String]
	}
	type ExtraChoreType {
    id: ID
    rewardAmount: String
    rewardMinute: String
		todo: String
    done: Boolean
    repeat: Boolean
  }
	type SubjectType {
		id: ID,
		name: String
		levelName: String
		hardnessMin: String
		hardnessMax: String
		amountOfQuestion: String
	}
	type FamilyTimeType {
		id:ID
		todo: String
		day: String
		time: String
  }
  
  type BasicObjectType {
    used: Boolean
    useGameTime: Boolean
    useMoney: Boolean
    point: Int
    rewardMinute: Int
    rewardAmount: Int
  }

  type dailyChoreType {
    id: ID
    choreName: String
    choreOpen: String
    choreClose: String
    chores: [String]
  }

	type User {
		id: ID
		familyName: String!
		password: String!
		email: String! @unique
    houserules: [String]
    rewardDay: String
    dailyChoreData: [dailyChoreType]
		choreTodo: [ChoreTodoType]
		extraChore: [ExtraChoreType]
		familyTime: [FamilyTimeType]
		joinDate: String
		avatar: String
		kids: [KidType]
		rewards: [RewardType]
		subjects:[SubjectType]
    type: String
    study: BasicObjectType
    dailyChores: BasicObjectType
    bonusChores: BasicObjectType
	}

	type TokenType { 
		token: String 
    type: String 
    message: String
    success: Boolean!
	}

	type TokenKidType { 
		token: String
    type: String
    message: String
    success: Boolean!
		username: String 
	}

	type Query {
		getCurrentUser: User
	}
	
	input ChoreInput {
		rewardOpenFrom: String
		rewardClosed: String
		toDo: String!
		type: String!
	}

	input SubjectInput {
		name: String
		levelName: String,
		hardnessMin: String,
		hardnessMax: String,
		amountOfQuestion: String
	}

	input colorInput {
		primary: String
		secondary: String
	}

	type Mutation {

		addHouseRule(
			houserules: [String!]
		): User

		addChore(
			name: String!
			end: String!
			start: String!
			points: String!
			todos: [String!]
			connectedKids: [String!]
		): User

		addFamilyMember(
			name: String!
			pass: String!
			username: String!
			chores: [ChoreInput]
		): User

		updateChildPoint(
			end: String!
			start: String!
			title: String!
			password: String!
      month: String!
      gameTime: Int!
			color: [colorInput]
		): User

		addKidSubjects(
			subject: [SubjectInput]	
		): User

		useChildPoint(
			pointsToUse: Int!
			password: String!
		): User

		savePointTillNextMonthChildPoint(
			bonusPoint: Int!
			password: String!
		): User

		signupUser(
			familyName: String!
			email: String! 
			password: String!
		): TokenType

		signinUser(
			email: String! 
			password: String!
		): TokenType

		loginKid(
			id: String!
			pass: String!
		): TokenKidType
		
		userAvatar(
			file: String!
		): User
	}
`;
